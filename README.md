Sam RMA system
================

This package manages complaints in SAM applications.

![Totem.com.pl](https://www.totem.com.pl/wp-content/uploads/2016/06/logo.png)

General System Requirements
-------------
- [PHP >7.4.0](http://php.net/)
- [Laravel ~6.*](https://github.com/laravel/framework)
- [SAM-core ~1.*](https://bitbucket.org/rudashi/samcore)
- [SAM-Admin ~1.*](https://bitbucket.org/rudashi/samadmin)
- [SAM-Acl ~1.*](https://bitbucket.org/rudashi/samacl)

Quick Installation
-------------
If necessary, use the composer to download the library

```
$ composer require totem-it/sam-complaints
```

Remember to put repository in the composer.json

```
"repositories": [
    {
        "type": "vcs",
        "url":  "https://bitbucket.org/rudashi/samcomplaints.git"
    }
],
```

Usage
-------------

###Protocols
To send daily protocols to managers, create new task in sam-tasks module with script:
```
\Totem\SamComplaints\App\Services\ProtocolService@notifyManagers
```

###API

Endpoints use standard filtering from `SAM-core`.


Authors
-------------

* **Borys Żmuda** - Lead designer - [LinkedIn](https://www.linkedin.com/in/boryszmuda/), [Portfolio](https://rudashi.github.io/)
