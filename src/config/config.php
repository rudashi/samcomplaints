<?php

return [

    'COST_LIMIT' => [

        /**
         * Complaint cost limit ("total_discount") in case of informing the Sales Director.
         * Value in PLN
         */
        'SALE_DIRECTOR' => 500,

        /**
         * Complaint cost limit ("total_discount") in case of informing the CEO.
         * Value in PLN
         */
        'CEO' => 1000,
    ],

    'DEFECTS' => [
        /**
         * Defects ID needed to inform logistic manager.
         */
        'DAMAGED_DURING_TRANSPORT' => [ 64, 65, 66, 102, 104, 129 ],
    ],

    /**
     * Turn on/off shipping confirmation in workflow.
     */
    'shipping_confirm' => false,

    /**
     * Numer of days needed to send reminder to make action on complaint.
     */
    'reminder' => 7,

];
