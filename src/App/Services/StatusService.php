<?php

namespace Totem\SamComplaints\App\Services;

use Illuminate\Http\Request;
use Totem\SamComplaints\App\Enums\AcceptanceType;
use Totem\SamComplaints\App\Enums\ClaimType;
use Totem\SamComplaints\App\Enums\StatusType;
use Totem\SamComplaints\App\Exceptions\StatusException;
use Totem\SamComplaints\App\Exceptions\StatusValidationException;
use Totem\SamComplaints\App\Model\Complaint;

class StatusService
{
    private Request $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getRequestRules(int $status = null): StatusRulesService
    {
        return new StatusRulesService($this->request, $status);
    }

    public function determine(Complaint $complaint): int
    {
        if ($complaint->status) {
            if ((int) $this->request->input('status') === StatusType::CEO) {
                return $this->ceoFilled($complaint);
            }
            if ((int) $this->request->input('status') === StatusType::ToSaleFix && $complaint->isQualityControlFilled() === false) {
                return $this->newComplaint($complaint);
            }
            if ($complaint->status->value === StatusType::Verification) {
                return $this->next($complaint);
            }
            if ($complaint->status->value !== (int) $this->request->input('status')) {
                throw new StatusValidationException();
            }
            return $this->next($complaint);
        }
        return $this->newComplaint($complaint);
    }

    private function next(Complaint $complaint): int
    {
        switch ($complaint->status->value) {
            case StatusType::Verification :
                return $this->transportOrQualityControlFilled($complaint);
            case StatusType::Transport :
                return $this->transportFilled($complaint);
            case StatusType::QualityControl :
                return $this->qualityControlFilled($complaint);
            case StatusType::SaleDirector :
                return $this->saleDirectorFilled($complaint);
            case StatusType::ToSaleFix :
                return $this->saleFixFilled($complaint);
            case StatusType::Shipping :
                return $this->shippingFilled($complaint);
            case StatusType::Finance :
                return $this->financeFilled($complaint);
            case StatusType::ToFinished :
                return $this->saleFinishFilled();
            default:
                throw new StatusException();
        }
    }

    private function newComplaint(Complaint $complaint): int
    {
        if ($complaint->isEditionNeedReturn()) {
            return StatusType::Verification;
        }
        return StatusType::QualityControl;
    }

    private function transportOrQualityControlFilled(Complaint $complaint): int
    {
        if ((int) $this->request->input('status') === StatusType::QualityControl) {
            return $this->qualityControlFilled($complaint);
        }
        if ((int) $this->request->input('status') === StatusType::Transport) {
            return $this->transportFilled($complaint);
        }
        throw new StatusValidationException();
    }

    private function transportFilled(Complaint $complaint): int
    {
        if ($complaint->isQualityControlFilled()) {
            return $this->qualityControlApproved($complaint);
        }
        return StatusType::QualityControl;
    }

    private function qualityControlFilled(Complaint $complaint): int
    {
        if ($complaint->isEditionNeedReturn() === false) {
            return $this->qualityControlApproved($complaint);
        }
        if ($complaint->isEditionNeedReturn()) {
            $complaint->load('deliveries');
            if ($complaint->isTransportFilled()) {
                return $this->qualityControlApproved($complaint);
            }
        }
        return StatusType::Transport;
    }

    private function qualityControlApproved(Complaint $complaint): int
    {
        if ($complaint->qc_approved->value === AcceptanceType::Accept) {
            if ($complaint->total_discount >= config('sam-complaints.COST_LIMIT.SALE_DIRECTOR')) {
                return StatusType::SaleDirector;
            }
            if ($complaint->claim->value === ClaimType::ToDetermine) {
                return StatusType::SaleDirector;
            }
            return $this->afterSaleDirector($complaint);

        }
        return StatusType::SaleDirector;
    }

    private function saleDirectorFilled(Complaint $complaint): int
    {
        if ($complaint->sale_approved->value === AcceptanceType::Accept) {
            return $this->afterSaleDirector($complaint);
        }
        if ($complaint->sale_approved->value === AcceptanceType::Decline) {
            return StatusType::Rejected;
        }
        if ($complaint->sale_approved->value === AcceptanceType::ToFix) {
            return StatusType::ToSaleFix;
        }
        if ($complaint->sale_approved->value === AcceptanceType::Fixed) {
            return $this->qualityControlFilled($complaint);
        }
        return $this->afterSaleDirector($complaint);
    }

    private function afterSaleDirector(Complaint $complaint): int
    {
        if (config('sam-complaints.shipping_confirm') && $complaint->isEditionNeedReturn() && $complaint->isEditionReturnFilled() === false) {
            return StatusType::Shipping;
        }
        return $this->afterShipping($complaint);
    }

    public function shippingFilled(Complaint $complaint): int
    {
        return $this->afterShipping($complaint);
    }

    private function afterShipping(Complaint $complaint): int
    {
        if (in_array($complaint->claim->value, [ClaimType::CancelOrder, ClaimType::NoteWithoutClaims], true)) {
            return StatusType::Completed;
        }
        if (in_array($complaint->claim->value, [ClaimType::QuotaDiscount, ClaimType::DiscountQty, ClaimType::ReprintAndDiscount, ClaimType::PercentDiscount], true)) {
            return StatusType::Finance;
        }
        if ($complaint->claim->value === ClaimType::ToDetermine) {
            return StatusType::ToSaleFix;
        }
        return StatusType::ToFinished;
    }

    public function saleFixFilled(Complaint $complaint): int
    {
        if ($complaint->isEditionNeedReturn()) {
            return $this->qualityControlFilled($complaint);
        }
        return $this->afterSaleDirector($complaint);
    }

    private function financeFilled(Complaint $complaint): int
    {
        if (in_array($complaint->claim->value, [ClaimType::RemoveDefects, ClaimType::Reprint, ClaimType::ReprintAndDiscount], true)) {
            return StatusType::ToFinished;
        }
        return StatusType::Completed;
    }

    private function saleFinishFilled(): int
    {
        return StatusType::Completed;
    }

    private function ceoFilled(Complaint $complaint): int
    {
        switch ($complaint->ceo_approved->value) {
            case AcceptanceType::Decline:
                return StatusType::Rejected;
            case AcceptanceType::ToFix:
                return StatusType::ToSaleFix;
            case AcceptanceType::Accept:
                return $complaint->status->value;
            default:
                throw new StatusValidationException();
        }
    }

}
