<?php

namespace Totem\SamComplaints\App\Services;

use Barryvdh\Snappy\PdfWrapper;
use Totem\SamComplaints\App\Model\Complaint;

class ComplaintLabelFile
{

    private Complaint $complaint;

    public function __construct(Complaint $complaint)
    {
        $this->complaint = $complaint;
    }

    public function makePDF(): string
    {
        /** @var $pdf PdfWrapper */
        $pdf = app(PdfWrapper::class);
        $pdf->loadView('sam-complaints::pdf.label', [
            'complaint' => $this->complaint,
            'url' => config('app.url').'/t/complaint/'.$this->complaint->uuid
        ])
            ->setPaper('A4', 'Landscape')
            ->setOption('encoding', 'utf-8');

        return $pdf->output();
    }

}
