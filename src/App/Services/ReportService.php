<?php

namespace Totem\SamComplaints\App\Services;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as BaseCollection;
use Totem\SamComplaints\App\Repositories\Contracts\ComplaintDefectRepositoryInterface;
use Totem\SamCore\App\Traits\Jsonable;

class ReportService implements Arrayable
{
    use Jsonable;

    public Collection $defect_place;
    public Collection $status;
    public array $claim;
    public array $operator;
    public BaseCollection $defect;
    public array $machines;
    public array $user;
    public array $customer;

    public function __construct(ComplaintDefectRepositoryInterface $repository, array $date_range, string $model_name)
    {
        [$first, $second] = $this->validateDates($date_range);

        $this->create($repository->fetchDataForCharts($model_name, $first, $second));
    }

    private function validateDates(array $dates): array
    {
        if (!is_array($dates) && count($dates) === 2) {
            throw new \InvalidArgumentException(__('Incorrect date parameters.'));
        }
        if (strtotime($dates[0]) === false || strtotime($dates[1]) === false) {
            throw new \InvalidArgumentException(__('Incorrect date parameters.'));
        }

        return $dates;
    }

    private function create(Collection $collection): void
    {
        $groupDefectPlace = $collection->groupBy('defect.defect_place_name');
        $groupMachines = $collection->groupBy('machine.name')->filter(fn($c, $k) => (bool) $k);
        $groupUsers = $collection->groupBy('user.fullname')->filter(fn($c, $k) => (bool) $k);
        $complaints = $collection->unique('complaint.uuid');

        $this->defect_place = $collection->countBy('defect.defect_place_name');
        $this->status = $complaints->countBy('complaint.status.description');
        $this->claim = [
            'count' => $complaints->countBy('complaint.claim.description'),
//            'cost' => $complaints->groupBy('complaint.claim.description')->map->sum('complaint.total_discount'),
        ];
        $this->operator = [
            'count' => $complaints->countBy('complaint.user.fullname'),
//            'cost' => $complaints->groupBy('complaint.user.fullname')->map->sum('complaint.total_discount'),
        ];
        $this->defect = $groupDefectPlace->map(fn($c) => $c->countBy('defect.name'));
        $this->machines = [
            'group' => $groupDefectPlace->map(fn($c) => $c->filter(fn($i) => $i->machine_id !== null)->countBy('machine.name'))->filter(fn($c) => count($c) > 0),
            'count' => $groupMachines->map->count(),
            'cost' => $groupMachines->map(fn($c) => (string) $c->sum('complaint.total_discount')),
        ];
        $this->user = [
            'group' => $groupDefectPlace->map(fn($c) => $c->filter(fn($i) => $i->user_id !== null)->countBy('user.fullname'))->filter(fn($c) => count($c) > 0),
            'count' => $groupUsers->map->count(),
            'cost' => $groupUsers->map(fn($c) => (string) $c->sum('complaint.total_discount')),
        ];
        $this->customer = [
            'count' => $complaints->countBy('complaint.customer'),
            'cost' => $complaints->groupBy('complaint.customer')->map(fn($c) => (string) $c->sum('complaint.total_discount')),
        ];
    }

}
