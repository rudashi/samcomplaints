<?php

namespace Totem\SamComplaints\App\Services;

use Illuminate\Support\Facades\Notification;
use Totem\SamComplaints\App\Model\Complaint;
use Totem\SamComplaints\App\Enums\StatusType;
use Totem\SamComplaints\App\Enums\AcceptanceType;
use JanRejnowski\SamDefects\App\Enums\DefectPlaceEnum;
use Totem\SamComplaints\App\Notifications\CapaActionNeededNotify;
use Totem\SamComplaints\App\Notifications\ComplaintCreated;
use Totem\SamComplaints\App\Notifications\ComplaintLabel;
use Totem\SamComplaints\App\Notifications\ComplaintPrepareShipping;
use Totem\SamComplaints\App\Notifications\ComplaintRequestToUpdate;
use Totem\SamComplaints\App\Notifications\ComplaintUpdated;
use Totem\SamComplaints\App\Notifications\ComplaintCanceled;
use Totem\SamComplaints\App\Notifications\ComplaintRejected;
use Totem\SamComplaints\App\Notifications\ComplaintOverCost;
use Totem\SamComplaints\App\Notifications\ComplaintCompleted;
use Totem\SamComplaints\App\Notifications\ComplaintSubcontractor;
use Totem\SamComplaints\App\Notifications\DamagedDuringTransportNotify;
use Totem\SamComplaints\App\Notifications\ComplaintQualityControlAccepted;
use Totem\SamComplaints\App\Notifications\ComplaintQualityControlRejected;
use Totem\SamAcl\App\Repositories\Contracts\PermissionRepositoryInterface;
use Totem\SamComplaints\App\Notifications\DefectBySubcontractor;

class NotificationService
{
    private PermissionRepositoryInterface $permission;

    public function __construct(PermissionRepositoryInterface $permission)
    {
        $this->permission = $permission;
    }

    public function created(Complaint $complaint): void
    {
        Notification::send($complaint->user, new ComplaintLabel($complaint, new ComplaintLabelFile($complaint)));
        Notification::send($complaint->user, new ComplaintCreated($complaint));
        Notification::send($this->getUsers($complaint->status->value), new ComplaintCreated($complaint));
        $this->sendToCEO($complaint);
    }

    public function updated(Complaint $complaint): void
    {
        $this->sendToOwner($complaint);

        if (in_array($complaint->status->value, [StatusType::ToSaleFix, StatusType::ToFinished], true) === false) {
            $this->sendUpdateTo($complaint->status->value, $complaint);
        }
        if ($complaint->getOriginal('status') === StatusType::ToSaleFix) {
            $this->sendToCEO($complaint);
        }
    }

    public function deleted(Complaint $complaint): void
    {
        $complaint->user->notify(new ComplaintCanceled($complaint));
    }

    public function noticeByDefect(Complaint $complaint): void
    {
        $complaint->load('defects.defect', 'defects.machine', 'defects.user');

        $complaint->defects->groupBy('defect.defect_place')->mapWithKeys(function($defects, int $defect_place) use ($complaint) {
            switch ($defect_place) {
                case DefectPlaceEnum::SalesRepresentative:
                case DefectPlaceEnum::Technology:
                case DefectPlaceEnum::Planing:
                case DefectPlaceEnum::Printing:
                case DefectPlaceEnum::Bindery:
                case DefectPlaceEnum::Expedition:
                case DefectPlaceEnum::Storehouse:
                case DefectPlaceEnum::Supplier:
                case DefectPlaceEnum::Profis:
                    Notification::route('mail', $this->getEmails($defect_place))->notify(new CapaActionNeededNotify($complaint, $defects));
                    break;
                case DefectPlaceEnum::Subcontractor:
                        Notification::send($this->getUsers(StatusType::Finance), new ComplaintSubcontractor($complaint));
                        Notification::route('mail', $this->getEmails($defect_place))->notify(new DefectBySubcontractor($complaint, $defects));
                    break;
                default:
                    break;
            }
            return [$defect_place => $defects];
        });

        $logistic = $complaint->defects->whereIn('defect_id', config('sam-complaints.DEFECTS.DAMAGED_DURING_TRANSPORT'));
        if ($logistic->isNotEmpty()) {
            Notification::send($this->getUsers(StatusType::Transport), new DamagedDuringTransportNotify($complaint, $logistic));
        }

    }

    public function notifyOwner(string $argument, Complaint $complaint): void
    {
        if ($argument === 'run_return_request') {
            $complaint->user->notify(new ComplaintRequestToUpdate($complaint));
        }
    }

    public function notifyShipping(Complaint $complaint): void
    {
        $deliveries = $complaint->deliveries->whereIn('country', ['Polska']);

        if ($deliveries->isNotEmpty()) {
            Notification::send($this->getUsers(StatusType::Shipping), new ComplaintPrepareShipping($complaint, $deliveries));
        }
    }

    private function sendToCEO(Complaint $complaint): void
    {
        if ($complaint->total_discount >= config('sam-complaints.COST_LIMIT.CEO')) {
            Notification::send($this->getUsers(StatusType::CEO), new ComplaintOverCost($complaint));
        }
    }

    private function sendToOwner(Complaint $complaint): void
    {
        if ($complaint->qc_approved !== null && $complaint->getOriginal('status') === StatusType::QualityControl) {
            if ($complaint->qc_approved->value === AcceptanceType::Accept) {
                $complaint->user->notify(new ComplaintQualityControlAccepted($complaint));
            }
            if ($complaint->qc_approved->value === AcceptanceType::Decline) {
                $complaint->user->notify(new ComplaintQualityControlRejected($complaint));
            }
        }
        if ($complaint->status->value === StatusType::Completed) {
            $complaint->user->notify(new ComplaintCompleted($complaint));
        }
        if ($complaint->status->value === StatusType::Rejected) {
            $complaint->user->notify(new ComplaintRejected($complaint));
        }
        if (in_array($complaint->status->value, [StatusType::ToSaleFix, StatusType::ToFinished], true)) {
            $complaint->user->notify(new ComplaintUpdated($complaint));
        }
    }

    private function sendUpdateTo(int $status, Complaint $complaint): void
    {
        Notification::send($this->getUsers($status), new ComplaintUpdated($complaint));
    }

    private function getUsers(string $context): \Illuminate\Database\Eloquent\Collection
    {
        $query = $this->permission->findBySlug('complaints.status.'.$context, ['users', 'roles.users'], ['id']);
        return $query->users->merge($query->roles->pluck('users')->collapse());
    }

    private function getEmails(int $defect_id): array
    {
        $emails = config('sam-defects.emails.'.$defect_id);

        if ($emails === null) {
            return [];
        }
        if (is_array($emails)) {
            return $emails;
        }
        return [$emails];
    }

}
