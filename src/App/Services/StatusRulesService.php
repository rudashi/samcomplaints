<?php

namespace Totem\SamComplaints\App\Services;

use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Validation\Rule;
use Totem\SamComplaints\App\Enums\AcceptanceType;
use Totem\SamComplaints\App\Enums\ClaimType;
use Totem\SamComplaints\App\Enums\StatusType;
use Totem\SamComplaints\App\Enums\TransportType;

class StatusRulesService
{
    private Request $request;
    private array $rules = [];

    public function __construct(Request $request, int $status = null)
    {
        $this->request = $request;

        if ($status) {
            $this->rules = $this->getRequestRules($status);
        }
    }

    public function getRules(): array
    {
        return $this->rules;
    }

    public function validated($status): Collection
    {
        return new Collection(array_flip(
            array_keys($this->getRequestRules($status instanceof StatusType ? $status->value : $status))
        ));
    }

    public function getRequestRules(int $status = null): array
    {
        switch ($status) {
            case StatusType::QualityControl:
                return $this->rulesForQualityControl();
            case StatusType::Transport:
                return $this->rulesForTransport();
            case StatusType::SaleDirector:
                return $this->rulesForSaleDirector();
            case StatusType::ToSaleFix:
                return $this->rulesForSaleFixing();
            case StatusType::Finance:
                return $this->rulesForFinance();
            case StatusType::Shipping:
                return $this->rulesForShipping();
            case StatusType::ToFinished:
                return $this->rulesForSaleFinishing();
            case StatusType::CEO:
                return $this->rulesForCEO();
            default:
                return [];
        }
    }

    private function rulesForQualityControl(): array
    {
        if ($this->request->input('run_return_request') === true) {
            return [];
        }

        return [
            'run_return_rest'               => ['required', 'numeric', Rule::in(AcceptanceType::Decline, AcceptanceType::Accept)],
            'run_return_rest_id'            => ['nullable', 'numeric', Rule::requiredIf($this->request->input('run_return_rest') === 1)],
            'run_return_rest_qty'           => ['nullable', 'numeric', Rule::requiredIf($this->request->input('run_return_rest') === 1)],
            'qc_approved'                   => ['required', 'numeric', Rule::in(AcceptanceType::Decline, AcceptanceType::Accept)],
            'defects'                       => ['required', 'array'],
            'defects.*.defect_id'           => ['required', 'required_with:defects.*.description'],
            'defects.*.description'         => 'nullable',
            'defects.*.machine_id'          => 'nullable',
            'defects.*.user_id'             => 'nullable',
            'defects.*.note'                => 'nullable',
            'defects.*.preventive_actions'  => 'nullable',
        ];
    }

    private function rulesForTransport(): array
    {
        if (is_array($this->request->input('deliveries')) && count($this->request->input('deliveries')) < 1) {
            return [
                'transport_cost'    => 'required',
                'transport_method'  => ['required', 'numeric', new EnumValue(TransportType::class)],
            ];
        }
        return [
            'run_return'                    => ['required', 'numeric', Rule::in(AcceptanceType::Accept)],
            'deliveries'                    => ['required', 'array'],
            'deliveries.*.transport_cost'   => 'required',
            'deliveries.*.transport_method' => ['required', 'numeric', new EnumValue(TransportType::class)],
            'deliveries.*.delivery_date'    => 'required',
        ];
    }

    private function rulesForSaleDirector(): array
    {
        return array_merge([
            'sale_approved'     => ['required', 'numeric', new EnumValue(AcceptanceType::class)],
            'sale_description'  => 'nullable',
        ], (int) $this->request->input('sale_approved') === AcceptanceType::Fixed ? $this->rulesForSaleFixing() : []);
    }

    private function rulesForSaleFixing(): array
    {
        return [
            'description'       => 'required',
            'run_return'        => 'numeric',
            'claim'             => ['required', 'numeric', new EnumValue(ClaimType::class)],
            'discount'          => 'required|numeric',
            'percentage'        => 'required|numeric',
            'qty_repair'        => 'required|numeric',
            'unit_price'        => 'required|numeric',
            'estimated_cost'    => 'required|numeric',
            'description_cost'  => 'nullable',
        ];
    }

    private function rulesForShipping(): array
    {
        return  [
            'confirm_return' => ['required', 'numeric', Rule::in(AcceptanceType::Accept)],
        ];
    }

    private function rulesForFinance(): array
    {
        return [
            'correction_invoice_number' => 'required',
            'correction_invoice_value'  => ['required', 'numeric'],
        ];
    }

    private function rulesForSaleFinishing(): array
    {
        return [
            'correction_order_number' => 'required',
        ];
    }

    private function rulesForCEO(): array
    {
        return [
            'ceo_approved'      => ['required', 'numeric', new EnumValue(AcceptanceType::class)],
            'ceo_description'   => 'nullable',
        ];
    }

}
