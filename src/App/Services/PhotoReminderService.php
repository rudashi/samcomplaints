<?php

namespace Totem\SamComplaints\App\Services;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Notification;
use Totem\SamComplaints\App\Enums\StatusType;
use Totem\SamComplaints\App\Model\Complaint;
use Totem\SamComplaints\App\Notifications\ComplaintPhotoReminder;

class PhotoReminderService
{

    private Complaint $model;
    private array $output = [];

    public function __construct(Complaint $model)
    {
        $this->model = $model;
    }

    public function notify(): string
    {
        $complaints = $this->model->newQuery()
            ->with('files', 'user')
            ->doesntHave('files')
            ->whereNull('qc_approved')
            ->where('run_return', 0)
            ->whereNotIn('status', [StatusType::Completed, StatusType::Canceled, StatusType::Rejected])
            ->get()
            ->groupBy('user_id')
        ;

        if ($complaints->isEmpty()) {
            return $this->printOutput();
        }

        $complaints->map(function(Collection $collection) {
            $notified = $collection->first()->user;

            $this->setOutput($notified->email, $collection);
            Notification::send($notified, new ComplaintPhotoReminder($collection));
        });

        return $this->printOutput();
    }

    private function setOutput(string $notifiable, Collection $collection): void
    {
        $collection->map(function(Complaint $item) use ($notifiable)  {
            $this->output[] = $notifiable.' - '.$item->complaint_number;
        });
    }

    private function printOutput(): string
    {
        if (count($this->output) === 0) {
            return 'No reminder to send.';
        }
        return json_encode($this->output);
    }

}
