<?php

namespace Totem\SamComplaints\App\Services;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Totem\SamComplaints\App\Model\Complaint;
use Totem\SamComplaints\App\Enums\StatusType;
use Totem\SamAcl\App\Repositories\Contracts\PermissionRepositoryInterface;
use Totem\SamMessenger\App\Repositories\Contracts\MessengerRepositoryInterface;

class ComplaintSummary
{
    private const PREFIX = 'RMA';
    private const SEPARATOR = '/';
    private const PAD = 5;

    private Complaint $model;
    private MessengerRepositoryInterface $messenger;
    private PermissionRepositoryInterface $permission;

    public function __construct(Complaint $model, MessengerRepositoryInterface $messenger, PermissionRepositoryInterface $permission)
    {
        $this->model        = $model;
        $this->messenger    = $messenger;
        $this->permission   = $permission;
    }

    public static function setPrimeIncome(float $total_price, float $total_cost): string
    {
        return sprintf('%.2f', $total_price - $total_cost);
    }

    public static function setTotalDiscount(float $discount, float $estimated_cost, float $currency_rate = 1): string
    {
        return sprintf('%.2f', ($discount * $currency_rate) + $estimated_cost);
    }

    public static function setBalance(float $prime_income, float $total_discount): string
    {
        return sprintf('%.2f', $prime_income - $total_discount);
    }

    public function setNumber(string $number): string
    {
        return $this->concatStrings(
            self::PREFIX,
            self::SEPARATOR,
            str_pad($number, self::PAD, 0, STR_PAD_LEFT),
            self::SEPARATOR,
            Carbon::now()->format('Y')
        );
    }

    public function getNewNumber(): int
    {
        $number = $this->model::query()->max('number');
        return $number ? $number + 1 : 1;
    }

    public function createThread(Complaint $complaint): void
    {
        $this->messenger->storeThread(new Request([
            'slug'          => Str::slug($complaint->complaint_number),
            'subject'       => __('Complaint'). ': '.$complaint->complaint_number,
            'model_id'      => $complaint->uuid,
            'model_type'    => get_class($complaint),
            'participants'  => $this->getUsers()
        ]));
    }

    public function archiveThread(Complaint $complaint): void
    {
        $this->messenger->markAsArchived(Str::slug($complaint->complaint_number));
    }

    public function setModifiablePermissions(Complaint $complaint): array
    {
        if (in_array($complaint->status->value, [StatusType::Canceled, StatusType::Completed, StatusType::Rejected], true)) {
            return [];
        }
        $permissions = [
            'complaints.status.'.$complaint->status->value,
            'complaints.status.'.StatusType::CEO,
        ];

        if (in_array($complaint->status->value, [StatusType::Verification, StatusType::QualityControl], true)) {
            $permissions[] = 'complaints.create';
        }

        return $permissions;
    }

    private function concatStrings(...$strings): string
    {
        return implode('', $strings);
    }

    private function getUsers(): array
    {
        $query = $this->permission->findBySlug('complaints.edit', ['users', 'roles.users'], ['id']);
        return $query->users->merge($query->roles->pluck('users')->collapse())->pluck('id')->toArray();
    }

}
