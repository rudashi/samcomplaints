<?php

namespace Totem\SamComplaints\App\Services;

use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Notification;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Barryvdh\Snappy\PdfWrapper;
use Illuminate\Foundation\Application;
use Totem\SamComplaints\App\Model\Complaint;
use Totem\SamComplaints\App\Model\ComplaintDefect;
use Totem\SamComplaints\App\Notifications\ComplaintsProtocols;
use Totem\SamComplaints\App\Repositories\Contracts\ComplaintDefectRepositoryInterface;
use Totem\SamComplaints\App\Repositories\Contracts\ComplaintRepositoryInterface;

class ProtocolService
{

    private Application $app;
    private ComplaintDefectRepositoryInterface $defect;
    private ComplaintRepositoryInterface $repository;
    private string $date_first;
    private string $date_second;
    private array $output = [];

    public function __construct(Application $app, ComplaintDefectRepositoryInterface $defect, ComplaintRepositoryInterface $repository)
    {
        $this->app = $app;
        $this->defect = $defect;
        $this->repository = $repository;
        $this->date_first = Carbon::yesterday()->toDateString();
        $this->date_second = $this->date_first;
    }

    public function notifyManagers(): string
    {
        $defects = $this->defect->getComplaintDefectsByDates($this->date_first, $this->date_second);
        $complaints = $this->repository->findWithRelationsByIds($defects->pluck('complaint_id'), ['files']);

        if ($complaints->isEmpty()) {
            return $this->printOutput();
        }

        $defects->groupBy('user.optima.manager.id')->map(function (EloquentCollection $defects) use ($complaints) {
            $attachments = new Collection([]);
            $defectComplaints = new Collection([]);
            $manager = $defects->first()->user->optima->manager;

            if ($manager) {
                $defects->map(function (ComplaintDefect $defect) use ($defectComplaints, $complaints, $attachments) {
                    $complaint = $complaints->firstWhere('uuid', $defect->complaint_id);
                    if ($complaint) {
                        $attachments->put(
                            $defect->id . '-' . $defect->complaint_id,
                            $this->makePDF($complaint, new Collection([$defect]))->output()
                        );
                        $defectComplaints->push($complaint);
                    }
                });
                $this->setOutput($manager->email, $defectComplaints);
                Notification::send($manager, new ComplaintsProtocols($defectComplaints, $attachments));
            }
        });

        return $this->printOutput();
    }

    private function makePDF(Complaint $complaint, Collection $defects): PdfWrapper
    {
        /** @var $pdf PdfWrapper */
        $pdf = $this->app->make(PdfWrapper::class);
        $pdf->loadView('sam-complaints::pdf.protocol', [
            'date' => Carbon::now(),
            'complaint' => $complaint,
            'defects' => $defects,
        ])
            ->setPaper('A4')
            ->setOption('encoding', 'utf-8');

        return $pdf;
    }

    public function streamPDF(Complaint $complaint, Collection $defects): \Illuminate\Http\Response
    {
        $pdf = $this->makePDF($complaint, $defects);

        return $pdf->inline(Str::kebab('complaint ' . $complaint->complaint_number . ' protocol') . '.pdf');

    }

    public function getComplaintDefectsByUuid(string $uuid): Collection
    {
        return $this->defect->getComplaintDefectsByUuid($uuid, get_class($this->repository->getModel()));
    }

    private function setOutput(string $notifiable, Collection $collection): void
    {
        $collection->map(function(Complaint $item) use ($notifiable)  {
            $this->output[] = $notifiable.' - '.$item->complaint_number;
        });
    }

    private function printOutput(): string
    {
        if (count($this->output) === 0) {
            return 'Nothing to send.';
        }
        return json_encode($this->output);
    }

}
