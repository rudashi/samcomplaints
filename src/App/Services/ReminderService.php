<?php

namespace Totem\SamComplaints\App\Services;

use Illuminate\Database\Query\Expression;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Notification;
use Totem\SamAcl\App\Model\Permission;
use Totem\SamComplaints\App\Enums\StatusType;
use Totem\SamComplaints\App\Model\Complaint;
use Totem\SamComplaints\App\Notifications\ComplaintUpdateReminder;

class ReminderService
{

    private int $days;
    private Complaint $model;
    private Permission $permission;
    private array $output = [];

    public function __construct(Complaint $model, Permission $permission)
    {
        $this->days = config('sam-complaints.reminder', 7);
        $this->model = $model;
        $this->permission = $permission;
    }

    public function notify(): string
    {
        $complaints = $this->model->newQuery()
            ->with('user')
            ->whereNotIn('status', [StatusType::Canceled, StatusType::Rejected, StatusType::Completed])
            ->whereDate('updated_at', '<=', new Expression('DATE_SUB(SYSDATE(), INTERVAL '.$this->days.' DAY)'))
            ->get();

        if ($complaints->isEmpty()) {
            return $this->printOutput();
        }

        $status = $complaints->mapWithKeys(function($complaint) {
            return [$complaint->id => 'complaints.status.'.$complaint->status->value];
        });
        $users = $this->getUsers($status->unique()->toArray());

        $status->map(function($status, $id) use ($complaints, $users) {
            /** @var Collection $notified */
            $complaint = $complaints->firstWhere('id', $id);
            $notified = in_array($complaint->status->value, [StatusType::ToFinished, StatusType::ToSaleFix], true)
                ? [$complaint->user->email]
                : $users->get($status)->toArray();

            $this->setOutput(implode(', ', $notified), $complaint);
            Notification::route('mail', $notified)->notify(new ComplaintUpdateReminder($complaint));
        });
        return $this->printOutput();
    }

    private function getUsers(array $slugs): Collection
    {
        return $this->permission->newQuery()
            ->with([
                'users',
                'roles' => function(\Illuminate\Database\Eloquent\Relations\BelongsToMany $q) {
                    $q->where('slug', '<>', 'admin');
                },
                'roles.users',
            ])
            ->whereIn('slug', $slugs)
            ->get(['id', 'slug'])
            ->mapWithKeys(function($perm) {
                return [$perm->slug => $perm->users->merge($perm->roles->pluck('users')->collapse())->pluck('email')];
            });
    }

    private function setOutput(string $notifiable, Complaint $complaint): void
    {
        $this->output[] = $notifiable.' - '.$complaint->complaint_number;
    }

    private function printOutput(): string
    {
        if (count($this->output) === 0) {
            return 'No reminder to send.';
        }
        return json_encode($this->output);
    }

}
