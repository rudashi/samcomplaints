<?php

namespace Totem\SamComplaints\App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Totem\SamComplaints\App\Model\ComplaintDefect;

interface ComplaintDefectRepositoryInterface
{

    public function getModel(): ComplaintDefect;

    public function queryAllByDefects(array $relationship = [], array $columns = ['*']): Builder;

    public function getComplaintDefectsByUuid(string $uuid, string $model_type): Collection;

    public function getComplaintDefectsByDates(string $first, string $second): Collection;

}
