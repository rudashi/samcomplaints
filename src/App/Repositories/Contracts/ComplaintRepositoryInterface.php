<?php

namespace Totem\SamComplaints\App\Repositories\Contracts;

use Totem\SamComplaints\App\Model\Complaint;
use Totem\SamComplaints\App\Requests\ComplaintRequest;
use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;
use Totem\SamCore\App\Repositories\Contracts\SimpleRepositoryInterface;

interface ComplaintRepositoryInterface extends RepositoryInterface, SimpleRepositoryInterface
{

    public function canModify();

    public function findWithRelationsByUuid(string $uuid = null, array $relationships = [], array $columns = ['*']): Complaint;

    public function storeNew(ComplaintRequest $request): Complaint;

    public function updateByUUID(ComplaintRequest $request, string $uuid = null): Complaint;

    public function deleteUUID(string $uuid): Complaint;

}
