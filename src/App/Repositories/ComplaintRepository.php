<?php

namespace Totem\SamComplaints\App\Repositories;

use Totem\SamComplaints\App\Enums\StatusType;
use Totem\SamComplaints\App\Model\Complaint;
use Totem\SamComplaints\App\Requests\ComplaintRequest;
use Totem\SamCore\App\Exceptions\RepositoryException;
use Totem\SamCore\App\Repositories\BaseRepository;
use Totem\SamComplaints\App\Repositories\Contracts\ComplaintRepositoryInterface;

/**
 * @property \Illuminate\Database\Eloquent\Builder|Complaint model
 */
class ComplaintRepository extends BaseRepository implements ComplaintRepositoryInterface
{

    private bool $can_modify = false;

    public function model(): string
    {
        return Complaint::class;
    }

    public function canModify(): self
    {
        $this->can_modify = true;

        return $this;
    }

    private function resetCanModify(): self
    {
        $this->can_modify = false;

        return $this;
    }

    public function findWithRelationsByUuid(string $uuid = null, array $relationships = [], array $columns = ['*']): Complaint
    {
        if ($uuid === null) {
            throw new RepositoryException(__('No complaint id have been given.') );
        }
        /** @var null|Complaint $data */
        $data = $this->model->where('uuid', $uuid)->first($columns);

        if ($data === null) {
            throw new RepositoryException(__('Given id :code is invalid or complaint not exist.', ['code' => $uuid]), 404);
        }

        if ($this->can_modify || $data->can_modify) {
            $this->resetCanModify();
            $data->load($relationships);
            return $data;
        }

        throw new RepositoryException(__('Given id :code is restricted.', ['code' => $uuid]), 403);

    }

    public function storeNew(ComplaintRequest $request): Complaint
    {
        $model = $this->model;
        $model->fill($request->validated());
        $model->save();

        return $model;
    }

    public function updateByUUID(ComplaintRequest $request, string $uuid = null): Complaint
    {
        $model = ($uuid === null) ? $this->model : $this->findWithRelationsByUuid($uuid);
        $model->fill($request->validated());

        if ($model->save()) {
            return $model;
        }
        throw new RepositoryException(__('There was a problem while saving. Report the problem to the administrator.'));
    }

    public function deleteUUID(string $uuid): Complaint
    {
        $model = $this->canModify()->findWithRelationsByUuid($uuid);

        if ($model->status->value === StatusType::Canceled) {
            throw new RepositoryException(__('Given id :code is invalid or complaint not exist.', ['code' => $uuid]), 404);
        }

        try {
            $model->delete();
            return $model;
        } catch (\Exception $exception) {
            throw new RepositoryException($exception->getMessage());
        }
    }

}
