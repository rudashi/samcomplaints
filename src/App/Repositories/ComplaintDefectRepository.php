<?php

namespace Totem\SamComplaints\App\Repositories;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;
use Totem\SamComplaints\App\Model\Complaint;
use Totem\SamComplaints\App\Model\ComplaintDefect;
use Totem\SamComplaints\App\Repositories\Contracts\ComplaintDefectRepositoryInterface;
use Totem\SamCore\App\Exceptions\RepositoryException;

class ComplaintDefectRepository implements ComplaintDefectRepositoryInterface
{

    private ComplaintDefect $model;

    public function __construct(ComplaintDefect $model)
    {
        $this->model = $model;
    }

    public function getModel(): ComplaintDefect
    {
        return $this->model;
    }

    public function queryAllByDefects(array $relationship = [], array $columns = ['*']): Builder
    {
        return $this->model::with($relationship)->select($columns);
    }

    public function getComplaintDefectsByUuid(string $uuid, string $model_type): Collection
    {
        return $this->model::with(['defect', 'user', 'machine'])
            ->whereNotNull('user_id')
            ->where(['complaint_type' => $model_type, 'complaint_id' => $uuid])
            ->get();
    }

    public function findByComplaintAndId(string $uuid, int $id, array $columns = ['*']): ComplaintDefect
    {
        /** @var null|ComplaintDefect $data */
        $data = $this->model->newQuery()->where(['id' => $id, 'complaint_id' => $uuid])->first($columns);

        if ($data === null) {
            throw new RepositoryException(__('Given id is invalid or complaint defect not exist.'), 404);
        }
        return $data;
    }

    public function getComplaintDefectsByDates(string $first, string $second): Collection
    {
        return $this->model::with(['defect', 'user.optima.manager', 'machine'])
            ->whereNotNull('user_id')
            ->whereDate($this->model->getCreatedAtColumn(), '>=', $first)
            ->whereDate($this->model->getCreatedAtColumn(), '<=', $second)
            ->get();
    }

    public function updateComplaintAndId(array $attributes, string $uuid, int $id): ComplaintDefect
    {
        $model = $this->findByComplaintAndId($uuid, $id);
        $model->fill($attributes);
        $model->save();

        return $model;
    }

    public function fetchDataForCharts(string $model_type, string $first, string $second)
    {
        return $this->queryAllByDefects([
                'complaint:uuid,created_at,status,customer,total_discount,claim,user_id',
                'complaint.user:id,firstname,lastname',
                'defect:id,name,defect_place',
                'machine:id,name',
                'user:id,firstname,lastname'
            ], ['id','complaint_id', 'complaint_type', 'defect_id', 'machine_id', 'user_id'])
            ->where('complaint_type', $model_type)
            ->whereHasMorph('complaint', Complaint::class, function (Builder $query) use ($first, $second) {
                $query->whereDate($this->model->getCreatedAtColumn(), '>=', $first)->whereDate($this->model->getCreatedAtColumn(), '<=', $second);
            })
            ->get()
        ;
    }

}
