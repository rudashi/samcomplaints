<?php

namespace Totem\SamComplaints\App\Requests;

use Illuminate\Validation\Rule;
use Totem\SamComplaints\App\Enums\AcceptanceType;
use Totem\SamComplaints\App\Enums\CapaType;
use Totem\SamComplaints\App\Enums\StatusCapa;
use Totem\SamCore\App\Requests\BaseRequest;

class CapaRequest extends BaseRequest
{

    public function rules(): array
    {
        switch ($this->input('action')) {
            case CapaType::Corrective:
                $rules = [
                    'status' => [
                        'required',
                        Rule::in(StatusCapa::toArray()),
                    ],
                    'summary_note' => 'nullable|string',
                    'summary_discount_type' => 'nullable|string',
                    'summary_discount' => 'nullable|numeric',
                ];
                break;
            case CapaType::Preventive:
                $rules = [
                    'status' => [
                        'required',
                        Rule::in(StatusCapa::toArray()),
                    ],
                    'opl' => Rule::in([AcceptanceType::Decline, AcceptanceType::Accept]),
                    'summary_note' => 'nullable|string',
                ];
                break;
            case CapaType::Additional:
                $rules = [
                    'protocol_at' => 'nullable|date_format:Y-m-d',
                    'month_billing' => 'nullable|date_format:Y-m',
                ];
                break;
            default:
                $rules = [];
        }
        return array_merge($rules, [
            'action' => 'required|enum_key:' . CapaType::class,
        ]);
    }

    public function messages(): array
    {
        return [
            'enum_key' => __('The :attribute field value you have entered is invalid.'),
        ];
    }

}
