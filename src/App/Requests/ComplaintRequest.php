<?php

namespace Totem\SamComplaints\App\Requests;

use BenSampo\Enum\Rules\EnumValue;
use Illuminate\Validation\Rule;
use Totem\SamComplaints\App\Enums\AcceptanceType;
use Totem\SamComplaints\App\Enums\ClaimType;
use Totem\SamComplaints\App\Enums\StatusType;
use Totem\SamComplaints\App\Services\StatusRulesService;
use Totem\SamCore\App\Requests\BaseRequest;

class ComplaintRequest extends BaseRequest
{

    protected array $sanitizeFields = [
        'description',
        'description_cost',
        'sale_description',
        'ceo_description',
    ];

    public function rules(): array
    {
        if ($this->missing('status') && $this->method() === 'POST') {
            return $this->rulesForCreate();
        }
        $rules = new StatusRulesService($this, (int) $this->input('status'));
        return array_merge([
            'status' => [ 'required', new EnumValue(StatusType::class, false) ]
        ], $rules->getRules());
    }

    public function attributes(): array
    {
        return [
            'reported_at' => __('Date reported'),
            'order_number' => __('Order number'),
            'order_name' => __('Order title'),
            'customer' => __('Customer'),
            'qty_total' => __('Edition'),
            'total_price' => __('Net price'),
            'total_cost' => __('Net cost'),
            'currency' => __('Currency'),
            'currency_rate' => __('Exchange rate'),
            'description' => __('Complaint description'),
            'claim' => __('Solution'),
            'discount' => __('Discount value'),
            'percentage' => __('Percent discount'),
            'qty_repair' => __('Edition to repair'),
            'unit_price' => __('Unit price'),
            'estimated_cost' => __('Net cost of the claim'),
            'files' => __('Files'),

            'qc_approved' => __('Acceptance of Quality Control'),
            'defects' => __('Defects List'),
            'defects.*.defect_id' => __('Defect'),
            'defects.*.description' => __('Description'),

            'transport_cost' => __('Transport cost'),
            'transport_method' => __('Pickup transport method'),

            'sale_approved' => __('Acceptance of Sale Director'),

            'correction_invoice_number' => __('Invoice correction number'),
            'correction_invoice_value'  => __('Invoice correction value'),

            'correction_order_number' => __('New order number'),

            'ceo_approved' => __('Acceptance of CEO'),

            'confirm_return' => __('Confirmation of edition withdrawal'),

            'deliveries.*.quantity' => __('Quantity'),
            'deliveries.*.tracking_number' => __('Tracking number'),
            'deliveries.*.shipping_date' => __('Shipping date'),
            'deliveries.*.name' => __('Name'),
            'deliveries.*.city' => __('City'),
            'deliveries.*.postal_code' => __('Post code'),
            'deliveries.*.street' => __('Street'),
            'deliveries.*.phone_number' => __('Phone'),
        ];
    }

    public function messages(): array
    {
        return [
            'status.required' => __('Control parameter missing.'),
            'defects.*.required_with' => __('The :attribute field is required.'),
            'deliveries.required' => __('The address is required to collect the package.'),
            'deliveries.*.*.required' => __('The :attribute field is required.'),
        ];
    }

    protected function prepareForValidation(): void
    {
        if ($this->has('claim')){
            $this->merge(['claim' => (int) $this->input('claim')]);
        }

        $this->resetUnnecessaryFields();

        if ((int) $this->input('status') === StatusType::ToSaleFix) {
            $this->merge([
                'discount'          => $this->input('discount', 0),
                'percentage'        => $this->input('percentage', 0),
                'qty_repair'        => $this->input('qty_repair', 0),
                'unit_price'        => $this->input('unit_price', 0),
                'estimated_cost'    => $this->input('estimated_cost', 0),
            ]);
        }

        if ($this->has('run_return')){
            $this->merge(['run_return' => (int) $this->input('run_return')]);
        }
        if ($this->has('run_return_rest')){
            $this->merge(['run_return_rest' => (int) $this->input('run_return_rest')]);
        }
        if ($this->has('qc_approved')) {
            $this->merge(['qc_approved' => (int) $this->input('qc_approved')]);
        }
        if ($this->has('sale_approved')) {
            $this->merge(['sale_approved' => (int) $this->input('sale_approved')]);
        }
        if ($this->has('ceo_approved')) {
            $this->merge(['ceo_approved' => (int) $this->input('ceo_approved')]);
        }
    }

    private function rulesForCreate() : array
    {
        return [
            'reported_at'       => 'required',
            'order_id'          => 'nullable',
            'order_number'      => 'required',
            'order_name'        => 'required',
            'customer'          => 'required',
            'qty_total'         => 'required|integer',
            'total_price'       => 'required|numeric',
            'total_cost'        => 'required|numeric',
            'currency'          => 'required',
            'currency_rate'     => 'required|numeric',
            'description'       => 'required',
            'run_return'        => ['required', 'numeric', Rule::in(AcceptanceType::Decline, AcceptanceType::Accept)],
            'claim'             => ['required', 'numeric', new EnumValue(ClaimType::class)],
            'discount'          => 'required|numeric',
            'percentage'        => 'required|numeric',
            'qty_repair'        => 'required|numeric',
            'unit_price'        => 'required|numeric',
            'estimated_cost'    => 'required|numeric',
            'description_cost'  => 'nullable',
            'invoice_number'    => 'nullable',

            'deliveries'                    => ['array', Rule::requiredIf($this->input('run_return') === 1)],
            'deliveries.*.shipping_id'      => 'nullable|integer',
            'deliveries.*.supplier'         => 'nullable|string',
            'deliveries.*.quantity'         => 'required|integer',
            'deliveries.*.tracking_number'  => 'required|string',
            'deliveries.*.shipping_date'    => 'required|date',
            'deliveries.*.name'             => 'required|string',
            'deliveries.*.city'             => 'required|string',
            'deliveries.*.postal_code'      => 'required|string',
            'deliveries.*.street'           => 'required|string',
            'deliveries.*.building_number'  => 'nullable|string',
            'deliveries.*.suite_number'     => 'nullable|string',
            'deliveries.*.country'          => 'nullable|string',
            'deliveries.*.phone_number'     => 'required|string',
            'deliveries.*.email'            => 'nullable|email',
            'deliveries.*.description'      => 'nullable|string',
        ];
    }

    private function resetUnnecessaryFields(): void
    {
        switch ($this->input('claim')) {
            case ClaimType::QuotaDiscount:
            case ClaimType::CancelOrder:
            case ClaimType::DiscountNextOrder:
                $this->merge([
                    'percentage' => 0,
                    'qty_repair' => 0,
                    'unit_price' => 0,
                    'estimated_cost' => 0,
                ]);
                break;
            case ClaimType::DiscountQty:
                $this->merge([
                    'percentage' => 0,
                    'estimated_cost' => 0,
                ]);
                break;
            case ClaimType::Reprint:
            case ClaimType::RemoveDefects:
                $this->merge([
                    'discount' => 0,
                    'percentage' => 0,
                    'unit_price' => 0,
                ]);
                break;
            case ClaimType::ReprintAndDiscount:
                $this->merge([
                    'percentage' => 0,
                    'unit_price' => 0,
                ]);
                break;
            case ClaimType::PercentDiscount:
                $this->merge([
                    'qty_repair' => 0,
                    'unit_price' => 0,
                    'estimated_cost' => 0,
                ]);
                break;
            case ClaimType::NoteWithoutClaims:
            case ClaimType::ToDetermine:
                break;
        }
    }

}
