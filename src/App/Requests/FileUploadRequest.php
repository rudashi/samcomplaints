<?php

namespace Totem\SamComplaints\App\Requests;

use Totem\SamCore\App\Requests\FileUploadRequest as BaseRequest;

class FileUploadRequest extends BaseRequest
{

    public function rules() : array
    {
        return [
            'file_data' => 'required|max:10000|mimes:jpeg,png,jpg,gif,pdf',
            'path'      => 'string',
        ];
    }

}