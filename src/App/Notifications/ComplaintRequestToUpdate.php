<?php

namespace Totem\SamComplaints\App\Notifications;

use Illuminate\Support\HtmlString;
use Illuminate\Notifications\Messages\MailMessage;

class ComplaintRequestToUpdate extends NotificationBase
{

    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject('[SAM] Prośba o modyfikację reklamacji '.$this->complaint->complaint_number)
            ->greeting(__('Hello!'))
            ->line(new HtmlString('<br>'))
            ->line(new HtmlString('W celu weryfikacji reklamacji do zlecenia <strong>'.$this->complaint->order_number.'</strong> Kontrola Jakości potrzebuje uszkodzonego egzemplarza.') )
            ->line(new HtmlString('Zaktualizuj reklamację wybierając <strong>Cofnięcie nakładu</strong> i podając adres odbioru egzemplarza.'))
            ->action(__('Check Complaint'), config('app.url').'/t/complaint/'.$this->complaint->uuid)
            ->line('Prosimy nie odpowiadać na tę wiadomość, ponieważ została wygenerowana automatycznie.')
        ;
    }

}
