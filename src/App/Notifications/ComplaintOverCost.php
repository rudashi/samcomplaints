<?php

namespace Totem\SamComplaints\App\Notifications;

use Illuminate\Support\HtmlString;
use Illuminate\Notifications\Messages\MailMessage;

class ComplaintOverCost extends NotificationBase
{

    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject('[SAM] Reklamacja '.$this->complaint->complaint_number)
            ->greeting(__('Hello :name', ['name' => $notifiable->fullname]))
            ->line(new HtmlString('<br>'))
            ->line(new HtmlString('Reklamacja do zlecenia <strong>'.$this->complaint->order_number.'</strong> może wymagać twojej interwencji.') )
            ->line(new HtmlString('<br>'))
            ->line('---')
            ->line(new HtmlString('<table style="width: 100%;">
            <tbody>
            <tr><td style="width: 40%;"><strong>'.__('Prime income').'</strong></td><td>'.$this->complaint->prime_income.' zł</td></tr>
            <tr><td style="width: 40%;"><strong>'.__('Complaint cost').'</strong></td><td>'.$this->complaint->total_discount.' zł</td></tr>
            <tr><td style="width: 40%;"><strong>'.__('Balance').'</strong></td><td>'.$this->complaint->balance.' zł</td></tr>
            </tbody>
            </table>'))
            ->line('---')
            ->line(new HtmlString('<br>'))
            ->line('Aktualny stan realizacji zgłoszenia można sprawdzić na stronie')
            ->action(__('Check Complaint'), config('app.url').'/t/complaint/'.$this->complaint->uuid)
            ->line('Prosimy nie odpowiadać na tę wiadomość, ponieważ została wygenerowana automatycznie.')
        ;
    }


}
