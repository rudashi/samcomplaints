<?php

namespace Totem\SamComplaints\App\Notifications;

use Illuminate\Support\HtmlString;
use Illuminate\Notifications\Messages\MailMessage;

class ComplaintSubcontractor extends NotificationBase
{

    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject('[SAM] Reklamacja '.$this->complaint->complaint_number)
            ->greeting(__('Hello :name', ['name' => $notifiable->fullname]))
            ->line(new HtmlString('<br>'))
            ->line(new HtmlString('Reklamacja do zlecenia <strong>'.$this->complaint->order_number.'</strong> jest powiązana z podwykonawcą.') )
            ->line('Jeżeli jest faktura od podwykonawcy, zgłoś to Dyrektorowi Sprzedaży.')
            ->line('Aktualny stan realizacji zgłoszenia można sprawdzić na stronie')
            ->action(__('Check Complaint'), config('app.url').'/t/complaint/'.$this->complaint->uuid)
            ->line('Prosimy nie odpowiadać na tę wiadomość, ponieważ została wygenerowana automatycznie.')
            ;
    }

}
