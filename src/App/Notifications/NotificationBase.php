<?php

namespace Totem\SamComplaints\App\Notifications;

use Illuminate\Bus\Queueable;
use Totem\SamComplaints\App\Model\Complaint;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

abstract class NotificationBase extends Notification implements ShouldQueue
{
    use Queueable;

    protected Complaint $complaint;

    public function __construct(Complaint $complaint)
    {
        $this->complaint = $complaint;
    }

    public function via(): array
    {
        return [
            'mail',
//            'database'
        ];
    }

    abstract public function toMail($notifiable): MailMessage;

    public function toDatabase(): array
    {
        return [
            'id'                => $this->complaint->id,
            'uuid'              => $this->complaint->uuid,
            'created_at'        => $this->complaint->created_at,
            'complaint_number'  => $this->complaint->complaint_number,
            'order_number'      => $this->complaint->order_number,
            'order_name'        => $this->complaint->order_name,
            'customer'          => $this->complaint->customer,
            'status'            => $this->complaint->status,
        ];
    }

}
