<?php

namespace Totem\SamComplaints\App\Notifications;

class ComplaintQualityControlAccepted extends ComplaintQualityControl
{

    protected bool $accepted = true;

}
