<?php

namespace Totem\SamComplaints\App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Collection;
use Illuminate\Support\HtmlString;
use Totem\SamComplaints\App\Model\Complaint;

class ComplaintPhotoReminder extends Notification implements ShouldQueue
{
    use Queueable;

    private Collection $complaints;

    public function __construct(Collection $complaints)
    {
        $this->complaints = $complaints;
    }

    public function via(): array
    {
        return [
            'mail',
        ];
    }

    public function toMail($notifiable): MailMessage
    {
        $message = new MailMessage;
        $message->subject('[SAM] Brak załączników w reklamacjach '.$this->complaints->implode('complaint_number', ', '))
            ->greeting(__('Hello :name', ['name' => $notifiable->fullname]))
            ->line(new HtmlString('<br>'))
            ->line('Przygotowaliśmy dla Ciebie zestawienie reklamacji z brakującymi załącznikami')
            ->line(new HtmlString('<br/>'))
            ->line('---')
            ->line(new HtmlString('<table style="width: 100%;"><tbody>'))
        ;

        $this->complaints->map(function(Complaint $complaint) use ($message) {
            $message->line(new HtmlString('<tr>
                <td style="border-bottom:1px solid #bbbfc3; border-right:1px solid #bbbfc3; padding: 5px;">'.$complaint->complaint_number.'</td>
                <td style="border-bottom:1px solid #bbbfc3; border-right:1px solid #bbbfc3; padding: 5px;">'.$complaint->order_number.'</td>
                <td style="border-bottom:1px solid #bbbfc3; border-right:1px solid #bbbfc3; padding: 5px;">'.$complaint->customer.'</td>
            </tr>'));
        });

        $message->line(new HtmlString('</tbody></table><br/>'))
            ->line('Prosimy nie odpowiadać na tę wiadomość, ponieważ została wygenerowana automatycznie.')
        ;

        return $message;
    }

    public function toDatabase(): array
    {
        return [];
    }

}
