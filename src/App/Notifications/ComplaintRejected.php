<?php

namespace Totem\SamComplaints\App\Notifications;

use Illuminate\Support\HtmlString;
use Illuminate\Notifications\Messages\MailMessage;
use Totem\SamComplaints\App\Enums\AcceptanceType;

class ComplaintRejected extends NotificationBase
{

    private array $who = ['name' => null, 'description' => null];

    public function toMail($notifiable): MailMessage
    {
        $this->setWho();

        return (new MailMessage)
            ->subject('[SAM] Reklamacja '.$this->complaint->complaint_number)
            ->greeting(__('Hello :name', ['name' => $notifiable->fullname]))
            ->line(new HtmlString('<br>'))
            ->line(new HtmlString('Reklamacja do zlecenia <strong>'.$this->complaint->order_number.'</strong> została <strong>odrzucona</strong>.') )
            ->line(new HtmlString('<br>'))
            ->line('---')
            ->line(new HtmlString('<table style="width: 100%;">
            <tbody>
            <tr><td style="width: 40%;"><strong>'.__($this->who['name']).'</strong></td><td>'.$this->who['description'].'</td></tr>
            </tbody>
            </table>'))
            ->line('---')
            ->line(new HtmlString('<br>'))
            ->line('Historię realizacji zgłoszenia można sprawdzić na stronie')
            ->action(__('Check Complaint'), config('app.url').'/t/complaint/'.$this->complaint->uuid)
            ->line('Prosimy nie odpowiadać na tę wiadomość, ponieważ została wygenerowana automatycznie.')
        ;
    }

    private function setWho(): void
    {
        if ($this->complaint->getAttributes()['sale_approved'] === AcceptanceType::Decline) {
            $this->who['name'] = 'Sale Director';
            $this->who['description'] = $this->complaint->sale_description;
        }
        if ($this->complaint->getAttributes()['ceo_approved'] === AcceptanceType::Decline) {
            $this->who['name'] = 'CEO';
            $this->who['description'] = $this->complaint->ceo_description;
        }
    }

}
