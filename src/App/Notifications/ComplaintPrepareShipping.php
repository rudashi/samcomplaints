<?php

namespace Totem\SamComplaints\App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Collection;
use Illuminate\Support\HtmlString;
use Totem\SamComplaints\App\Enums\TransportType;
use Totem\SamComplaints\App\Model\Complaint;
use Totem\SamComplaints\App\Model\ComplaintDelivery;

class ComplaintPrepareShipping extends NotificationBase
{
    private Collection $deliveries;
    private array $table_rows = [];

    public function __construct(Complaint $complaint, Collection $deliveries)
    {
        parent::__construct($complaint);
        $this->deliveries = $deliveries;
    }

    public function toMail($notifiable): MailMessage
    {
        $this->setTableRows();

        $message = new MailMessage;
        $message->subject('[SAM] Cofnięcie nakładu do reklamacji '.$this->complaint->complaint_number)
            ->greeting(__('Hello!'))
            ->line(new HtmlString('<br>'))
            ->line(new HtmlString('<table style="width: 100%;"><tbody>'))
        ;

        foreach($this->table_rows as $index => $row) {
            $message->line(new HtmlString('<tr>
                <td style="border-bottom:1px solid #bbbfc3; border-right:1px solid #bbbfc3; padding: 5px;">'.($index + 1).'</td>
                <td style="border-bottom:1px solid #bbbfc3; padding: 10px;">
                    <table><tbody>
                    <tr>
                        <td style="border-bottom:1px solid #bbbfc3; padding-bottom: 10px;">
                            <div>'.$row->quantity.'</div>
                            <div>'.$row->name.'</div>
                            <div>'.$row->address.'</div>
                            <div>'.$row->phone_number.'</div>
                            <div>'.$row->transport_method.'</div>
                        </td>
                    </tr>
                    <tr><td style="padding:10px;">'.$row->description.'</td></tr>
                    </tbody></table>
                </td>
            </tr>'));
        }

        $message->line(new HtmlString('</tbody></table>'))
            ->line(new HtmlString('<br>'))
            ->line('W razie wątpliwości, skontaktuj się z operatorem reklamacji.')
            ->action(__('Check Complaint'), config('app.url').'/t/complaint/'.$this->complaint->uuid)
            ->line('Prosimy nie odpowiadać na tę wiadomość, ponieważ została wygenerowana automatycznie.')
        ;

        return $message;
    }

    private function setTableRows(): void
    {
        $this->deliveries->map(function(ComplaintDelivery $delivery) {
            $this->table_rows[] = (object) [
                'quantity' => __('Quantity').': <b>'.$delivery->quantity.'</b>',
                'name' => __('Name').': <b>'.$delivery->name.'</b>',
                'address' => __('Address').': <b>'.$delivery->address.'</b>',
                'phone_number' => __('Phone').': <b>'.$delivery->phone_number.'</b>',
                'transport_method' => __('Pickup transport method').': <b>'.TransportType::getDescription($delivery->transport_method).'</b>',
                'description' => __('Description').': <b>'.$delivery->description.'</b>',
            ];
        });
    }
}
