<?php

namespace Totem\SamComplaints\App\Notifications;

use Barryvdh\Snappy\PdfWrapper;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\HtmlString;
use Illuminate\Notifications\Messages\MailMessage;
use Totem\SamComplaints\App\Model\Complaint;
use Totem\SamComplaints\App\Model\ComplaintDefect;

class DefectBySubcontractor extends NotificationBase
{

    private Collection $attachments;
    private Collection $defects;

    public function __construct(Complaint $complaint, Collection $defects)
    {
        parent::__construct($complaint);

        $this->defects = $defects;
        $this->prepareAttachments();
    }

    public function toMail($notifiable = null): MailMessage
    {
        $message = new MailMessage;

        $message->subject('[SAM] Reklamacja '.$this->complaint->complaint_number)
            ->greeting(__('Hello!'))
            ->line(new HtmlString('<br>'))
            ->line(new HtmlString('Reklamacja do zlecenia <strong>'.$this->complaint->order_number.'</strong> jest powiązana z podwykonawcą.') )
            ->line(new HtmlString('<br>'))
            ->line('---')
            ->line(new HtmlString('<table style="width: 100%;"><tbody>'))
            ->line(new HtmlString('<tr>
                <td style="border-bottom:1px solid #bbbfc3; border-right:1px solid #bbbfc3; padding: 5px;">'. __('Complaint number') .'</td>
                <td style="border-bottom:1px solid #bbbfc3;">'.$this->complaint->complaint_number.'</td>
            </tr>'))
            ->line(new HtmlString('<tr>
                <td style="border-bottom:1px solid #bbbfc3; border-right:1px solid #bbbfc3; padding: 5px;">'. __('Order number') .'</td>
                <td style="border-bottom:1px solid #bbbfc3;">'.$this->complaint->order_number.'</td>
            </tr>'))
            ->line(new HtmlString('<tr>
                <td style="border-bottom:1px solid #bbbfc3; border-right:1px solid #bbbfc3; padding: 5px;">'. __('Order title') .'</td>
                <td style="border-bottom:1px solid #bbbfc3;">'.$this->complaint->order_name.'</td>
            </tr>'))
            ->line(new HtmlString('<tr>
                <td style="border-bottom:1px solid #bbbfc3; border-right:1px solid #bbbfc3; padding: 5px;">'. __('Customer') .'</td>
                <td style="border-bottom:1px solid #bbbfc3;">'.$this->complaint->customer.'</td>
            </tr>'))
            ->line(new HtmlString('<tr>
                <td style="border-bottom:1px solid #bbbfc3; text-align: center; font-weight: bold; padding: 10px;" colspan="2">'. __('Defects') .'</td>
            </tr>'))
        ;

        $this->defects->map(function (ComplaintDefect $defect) use ($message) {
            $message->line(new HtmlString('<tr>
                <td style="border-bottom:1px solid #bbbfc3; border-right:1px solid #bbbfc3; padding: 5px; width: 40%">'.$defect->defect->name.'</td>
                <td style="border-bottom:1px solid #bbbfc3;">'.$defect->description.'</td>
            </tr>'));
        });

        $message->line(new HtmlString('</tbody></table>'));

        $message->line(new HtmlString('<br>'))
            ->line('Aktualny stan realizacji reklamacji można sprawdzić na stronie')
            ->action(__('Check Complaint'), config('app.url').'/t/complaint/'.$this->complaint->uuid)
            ->line('Prosimy nie odpowiadać na tę wiadomość, ponieważ została wygenerowana automatycznie.')
        ;

        foreach($this->attachments as $filename => $attachment) {
            $message->attachData(base64_decode($attachment), $filename.'.pdf', [
                'mime' => 'application/pdf',
            ]);
        }

        return $message;
    }

    private function prepareAttachments(): void
    {
        $this->attachments = new Collection([]);

        $this->defects->map(function (ComplaintDefect $defect) {
            $this->attachments->put(
                $defect->id . '-' . $defect->complaint_id,
                base64_encode($this->makePDF($this->complaint, new Collection([$defect]))->output())
            );
        });
    }

    private function makePDF(Complaint $complaint, Collection $defects): PdfWrapper
    {
        /** @var $pdf PdfWrapper */
        $pdf = app(PdfWrapper::class);
        $pdf->loadView('sam-complaints::pdf.subcontractor', [
            'date' => Carbon::now(),
            'complaint' => $complaint,
            'defects' => $defects,
            'author' => auth()->user()
        ])
            ->setPaper('A4')
            ->setOption('encoding', 'utf-8');

        return $pdf;
    }

}
