<?php

namespace Totem\SamComplaints\App\Notifications;

use Illuminate\Support\Collection;
use Illuminate\Support\HtmlString;
use Illuminate\Notifications\Messages\MailMessage;
use Totem\SamComplaints\App\Model\Complaint;

class DamagedDuringTransportNotify extends NotificationBase
{

    private Collection $defects;

    public function __construct(Complaint $complaint, Collection $defects)
    {
        parent::__construct($complaint);

        $this->defects = $defects;
    }

    public function toMail($notifiable): MailMessage
    {
        $message = new MailMessage;

        $message->subject('[SAM] Reklamacja '.$this->complaint->complaint_number)
            ->greeting(__('Hello :name', ['name' => $notifiable->fullname]))
            ->line(new HtmlString('<br>'))
            ->line(new HtmlString('Reklamacja do zlecenia <strong>'.$this->complaint->order_number.'</strong> została <strong>zaktualizowana</strong>.') )
            ->line(new HtmlString('<br>'))
            ->line('---')
            ->line('Podczas kontroli jakości wykryto nieprawidłowości związane z transportem.')
            ->line('Możesz zgłosić reklamację w ramach transportu u przewoźnika.')
            ->line('---')
            ->line(new HtmlString('<table style="width: 100%;"><tbody>'))
        ;

        foreach($this->defects as $row) {
            $message->line(new HtmlString('<tr>
                <td style="border-bottom:1px solid #bbbfc3; border-right:1px solid #bbbfc3; padding: 5px; width: 40%">'.$row->defect->name.'</td>
                <td style="border-bottom:1px solid #bbbfc3;">'.$row->description.'</td>
            </tr>'));
        }

        $message->line(new HtmlString('</tbody></table>'));

        $message->line(new HtmlString('<br>'))
            ->line('Aktualny stan realizacji reklamacji można sprawdzić na stronie')
            ->action(__('Check Complaint'), config('app.url').'/t/complaint/'.$this->complaint->uuid)
            ->line('Prosimy nie odpowiadać na tę wiadomość, ponieważ została wygenerowana automatycznie.')
        ;

        return $message;
    }

}
