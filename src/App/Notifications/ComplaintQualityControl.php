<?php

namespace Totem\SamComplaints\App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;
use JanRejnowski\SamDefects\App\Enums\DefectPlaceEnum;
use Totem\SamComplaints\App\Model\Complaint;

class ComplaintQualityControl extends NotificationBase
{

    protected bool $accepted = false;
    private array $table_rows = [];

    public function __construct(Complaint $complaint)
    {
        $complaint->load(['defects.defect', 'defects.machine', 'defects.user']);

        parent::__construct($complaint);
    }

    public function toMail($notifiable): MailMessage
    {
        $this->setTableRows();

        $message = new MailMessage;
        $message->subject('[SAM] Reklamacja '.$this->complaint->complaint_number)
            ->greeting(__('Hello :name', ['name' => $notifiable->fullname]))
            ->line(new HtmlString('<br>'))
            ->line(new HtmlString('Reklamacja do zlecenia <strong>'.$this->complaint->order_number.'</strong> została zweryfikowana przez Kontrolę Jakości.'))
        ;

        if ($this->accepted) {
            $message->line(new HtmlString('<div style="text-align: center;"><span class="button button-success">Reklamacja zasadna</span></div>'));
        } else{
            $message->line(new HtmlString('<div style="text-align: center;"><span class="button button-red">Reklamacja odrzucona</span></div>'));
        }

        $message->line(new HtmlString('<br>'))
            ->line('---')
            ->line(new HtmlString('<table style="width: 100%;"><tbody>'))
        ;

        foreach($this->table_rows as $index => $row) {
            $message->line(new HtmlString('<tr>
                <td style="border-bottom:1px solid #bbbfc3; border-right:1px solid #bbbfc3; padding: 5px;">'.($index + 1).'</td>
                <td style="border-bottom:1px solid #bbbfc3; padding: 10px;">
                    <table><tbody>
                    <tr>
                        <td style="border-bottom:1px solid #bbbfc3; padding-bottom: 10px;">
                            <div>'.$row->place.'</div><div>'.$row->name.'</div><div>'.$row->machine.'</div><div>'.$row->user.'</div>
                        </td>
                    </tr>
                    <tr><td style="padding:10px;">'.$row->description.'</td></tr>
                    </tbody></table>
                </td>
            </tr>'));
        }

        $message->line(new HtmlString('</tbody></table>'))
            ->line(new HtmlString('<br>'))
            ->line('Historię realizacji zgłoszenia można sprawdzić na stronie')
            ->action(__('Check Complaint'), config('app.url').'/t/complaint/'.$this->complaint->uuid)
            ->line('Prosimy nie odpowiadać na tę wiadomość, ponieważ została wygenerowana automatycznie.')
        ;

        return $message;
    }

    private function setTableRows(): void
    {
        $this->complaint->defects->map(function(\Totem\SamComplaints\App\Model\ComplaintDefect $defect) {
            $this->table_rows[] = (object) [
                'place' => __('Defect place').': <b>'.DefectPlaceEnum::getDescription($defect->defect->defect_place).'</b>',
                'name' => __('Defect').': <b>'.$defect->defect->name.'</b>',
                'machine' => $defect->machine ? __('Machine').': <b>'.$defect->machine->name.'</b>' : null,
                'user' => $defect->user ? __('Employee').': <b>'.$defect->user->fullname.'</b>' : null,
                'description' => $defect->description,
            ];
        });
    }

}
