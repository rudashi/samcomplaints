<?php

namespace Totem\SamComplaints\App\Notifications;

use Illuminate\Support\HtmlString;
use Illuminate\Notifications\Messages\MailMessage;

class ComplaintUpdated extends NotificationBase
{

    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject('[SAM] Reklamacja '.$this->complaint->complaint_number)
            ->greeting(__('Hello :name', ['name' => $notifiable->fullname]))
            ->line(new HtmlString('<br>'))
            ->line(new HtmlString('Reklamacja do zlecenia <strong>'.$this->complaint->order_number.'</strong> została <strong>zaktualizowana</strong>.') )
            ->line(new HtmlString('Otrzymała ona status <strong>'.$this->complaint->status->description.'</strong>.'))
            ->line('Aktualny stan realizacji zgłoszenia można sprawdzić na stronie')
            ->action(__('Check Complaint'), config('app.url').'/t/complaint/'.$this->complaint->uuid)
            ->line('Prosimy nie odpowiadać na tę wiadomość, ponieważ została wygenerowana automatycznie.')
            ;
    }

}
