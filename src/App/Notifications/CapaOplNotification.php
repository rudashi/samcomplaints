<?php

namespace Totem\SamComplaints\App\Notifications;

use Barryvdh\Snappy\PdfWrapper;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\HtmlString;
use Illuminate\Notifications\Messages\MailMessage;
use Totem\SamComplaints\App\Model\Complaint;
use Totem\SamComplaints\App\Model\ComplaintDefect;

class CapaOplNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private ComplaintDefect $capa;
    private Complaint $complaint;
    private Collection $attachments;

    public function __construct(ComplaintDefect $capa)
    {
        $this->capa = $capa;
        $this->complaint = $capa->complaint;
    }

    public function via(): array
    {
        return ['mail'];
    }

    public function toMail(): MailMessage
    {
        return (new MailMessage)
            ->subject('[SAM] Alert Jakości '.$this->complaint->complaint_number)
            ->greeting(__('Hello!'))
            ->line(new HtmlString('<br>'))
            ->line(new HtmlString('Wygenerowano Alert Jakości do zlecenia <strong>'.$this->complaint->order_number.'</strong>.') )
            ->line(new HtmlString('<br>'))
            ->line('---')
            ->line(new HtmlString('<br>'))
            ->line('Prosimy nie odpowiadać na tę wiadomość, ponieważ została wygenerowana automatycznie.')
            ->attachData($this->makePDF($this->complaint, $this->capa), 'alert-jakosci.pdf', [
                'mime' => 'application/pdf',
            ]);
    }

    public function makePDF(Complaint $complaint, ComplaintDefect $capa): string
    {
        /** @var $pdf PdfWrapper */
        $pdf = app(PdfWrapper::class);
        $pdf->loadView('sam-complaints::pdf.opl', [
            'date' => Carbon::now(),
            'complaint' => $complaint,
            'capa' => $capa,
            'author' => config('sam-defects.emails.'.$capa->defect->defect_place)[0],
        ])
            ->setPaper('A4')
            ->setOption('encoding', 'utf-8');

        return $pdf->output();
    }

}
