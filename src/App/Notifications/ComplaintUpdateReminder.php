<?php

namespace Totem\SamComplaints\App\Notifications;

use Illuminate\Support\HtmlString;
use Illuminate\Notifications\Messages\MailMessage;

class ComplaintUpdateReminder extends NotificationBase
{

    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject('[SAM] Przypomnienie o reklamacji '.$this->complaint->complaint_number)
            ->greeting(__('Hello!'))
            ->line(new HtmlString('<br>'))
            ->line(new HtmlString('Reklamacja do zlecenia <strong>'.$this->complaint->order_number.'</strong> oczekuje na aktualizację.') )
            ->line('Sprawdź czy nie została zapomniana.')
            ->action(__('Check Complaint'), config('app.url').'/t/complaint/'.$this->complaint->uuid)
            ->line('Prosimy nie odpowiadać na tę wiadomość, ponieważ została wygenerowana automatycznie.')
        ;
    }

}
