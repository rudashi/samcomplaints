<?php

namespace Totem\SamComplaints\App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;
use Totem\SamComplaints\App\Model\Complaint;
use Totem\SamComplaints\App\Services\ComplaintLabelFile;

class ComplaintLabel extends NotificationBase
{
    private ComplaintLabelFile $file;

    public function __construct(Complaint $complaint, ComplaintLabelFile $file)
    {
        parent::__construct($complaint);
        $this->file = $file;
    }

    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject('[SAM] Etykieta reklamacyjna '.$this->complaint->complaint_number)
            ->greeting(__('Hello!'))
            ->line(new HtmlString('<br>'))
            ->line(new HtmlString('W załączniku znajduje się etykieta reklamacyjna do zlecenia <strong>'.$this->complaint->order_number.'</strong>.') )
            ->line('Przekaż ją klientowi, aby nakleił na każdą przesyłkę.')
            ->line('Paczki bez etykiety mogą być zwrócone do nadawcy.')
            ->line('Prosimy nie odpowiadać na tę wiadomość, ponieważ została wygenerowana automatycznie.')
            ->attachData($this->file->makePDF(), 'Etykieta-reklamacyjna.pdf', [
                'mime' => 'application/pdf',
            ]);
    }

}
