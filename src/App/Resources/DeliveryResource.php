<?php

namespace Totem\SamComplaints\App\Resources;

use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property-read \Totem\SamComplaints\App\Model\ComplaintDelivery resource
 */
class DeliveryResource extends ApiResource
{

    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'shipping_id' => $this->resource->shipping_id,
            'supplier' => $this->resource->supplier,
            'quantity' => $this->resource->quantity,
            'tracking_number' => $this->resource->tracking_number,
            'shipping_date' => $this->resource->shipping_date ? $this->resource->shipping_date->format('Y-m-d') : null,
            'name' => $this->resource->name,
            'city' => $this->resource->city,
            'postal_code' => $this->resource->postal_code,
            'street' => $this->resource->street,
            'building_number' => $this->resource->building_number,
            'suite_number' => $this->resource->suite_number,
            'country' => $this->resource->country,
            'address' => $this->resource->address,
            'phone_number' => $this->resource->phone_number,
            'email' => $this->resource->email,
            'description' => $this->resource->description,
            'transport_cost' => $this->getNumberFormat($this->resource->transport_cost, 0),
            'transport_method' => $this->resource->transport_method,
            'delivery_date' => $this->resource->delivery_date === null ? null : $this->resource->delivery_date->format('Y-m-d'),
        ];
    }

}
