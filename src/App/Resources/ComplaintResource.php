<?php

namespace Totem\SamComplaints\App\Resources;

use Totem\SamCore\App\Resources\FileCollection;
use Totem\SamCore\App\Resources\ApiResource;

/** @property-read \Totem\SamComplaints\App\Model\Complaint $resource */
class ComplaintResource extends ApiResource
{

    public function toArray($request): array
    {
        return [
            'id'                    => $this->resource->uuid,
            'slug'                  => $this->resource->slug,
            'created'               => $this->resource->created_at->format('Y-m-d'),
            'reported'              => $this->resource->reported_at === null ? null : $this->resource->reported_at->format('Y-m-d'),
            'user' => $this->whenLoaded('user',  function() {
                return [
                    'id'            => $this->resource->user_id,
                    'fullname'      => $this->resource->user->fullname,
                ];
            }),
            'status'                => $this->resource->status,
            'can_modify'            => $this->resource->can_modify,
            'complaint_number'      => $this->resource->complaint_number,
            'summary' => [
                'prime_income'      => $this->getNumberFormat($this->resource->prime_income),
                'total_discount'    => $this->getNumberFormat($this->resource->total_discount),
                'balance'           => $this->getNumberFormat($this->resource->balance),
            ],
            'order' => [
                'order_id'          => $this->resource->order_id,
                'order_number'      => $this->resource->order_number,
                'order_name'        => $this->resource->order_name,
                'customer'          => $this->resource->customer,
                'qty_total'         => $this->resource->qty_total,
                'total_price'       => $this->getNumberFormat($this->resource->total_price),
                'total_cost'        => $this->getNumberFormat($this->resource->total_cost),
                'invoice_number'    => $this->resource->invoice_number,
            ],
            'currency'              => $this->resource->currency,
            'currency_rate'         => $this->resource->currency_rate,
            'description'           => $this->resource->description,
            'claim'                 => $this->resource->claim,
            'run_return'            => $this->resource->run_return,
            'discount'              => $this->getNumberFormat($this->resource->discount),
            'discount_currency'     => $this->getNumberFormat($this->resource->discount_currency),
            'percentage'            => $this->resource->percentage,
            'qty_repair'            => $this->resource->qty_repair,
            'unit_price'            => $this->getNumberFormat($this->resource->unit_price),
            'unit_price_currency'   => $this->getNumberFormat($this->resource->unit_price_currency),
            'estimated_cost'        => $this->getNumberFormat($this->resource->estimated_cost),
            'description_cost'      => $this->resource->description_cost,
            'qc_approved'           => $this->resource->qc_approved,
            'transport_cost'        => $this->getNumberFormat($this->resource->transport_cost),
            'transport_method'      => $this->resource->transport_method,
            'sale_approved'         => $this->resource->sale_approved,
            'sale_description'      => $this->resource->sale_description,
            'ceo_approved'          => $this->resource->ceo_approved,
            'ceo_description'       => $this->resource->ceo_description,
            'confirm_return'        => $this->resource->confirm_return,
            'correction' => [
                'invoice_number'    => $this->resource->correction_invoice_number,
                'invoice_value'     => $this->getNumberFormat($this->resource->correction_invoice_value),
                'order_number'      => $this->resource->correction_order_number,
            ],
            'files'                 => FileCollection::make($this->whenLoaded('files')),
            'defects'               => ComplaintDefectResource::collection($this->whenLoaded('defects')),
            'history'               => HistoryResource::collection($this->whenLoaded('history')),
            'deliveries'            => DeliveryResource::collection($this->whenLoaded('deliveries')),
        ];
    }

}
