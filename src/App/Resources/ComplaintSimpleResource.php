<?php

namespace Totem\SamComplaints\App\Resources;

use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property-read \Totem\SamComplaints\App\Model\Complaint $resource
 */
class ComplaintSimpleResource extends ApiResource
{

    public function toArray($request): array
    {
        return [
            'id'                => $this->resource->uuid,
            'slug'              => $this->resource->slug,
            'can_modify'        => $this->resource->can_modify,
            'complaint_number'  => $this->resource->complaint_number,
            'status'            => $this->resource->status,
            'user'              => $this->whenLoaded('user', function() {
                return $this->resource->user->fullname;
            }),
            'created'           => $this->resource->created_at->format('Y-m-d'),
            'date_life'         =>  trans_choice(__('{0} :count days|[1,1] :count day |[2,*] :count days'), $this->resource->date_life),
            'order_number'      => $this->resource->order_number,
            'customer'          => $this->resource->customer,
            'order_name'        => $this->resource->order_name,
            'prime_income'      => $this->resource->prime_income,
            'total_discount'    => $this->resource->total_discount,
            'balance'           => $this->resource->balance,
            'qc_approved'       => $this->resource->quality_control_approved,
            'claim'             => $this->resource->claim,
        ];
    }

}
