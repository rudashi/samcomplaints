<?php

namespace Totem\SamComplaints\App\Resources;

use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property-read \Rudashi\LaravelHistory\Models\History $resource
 */
class HistoryResource extends ApiResource
{

    static protected array $hide_fields = [
        'user_id',
        'number',
        'updated_at',
        'created_at',
        'prime_income',
        'total_discount',
        'balance'
    ];

    static protected array $currency_fields = [
        'total_price',
        'total_cost',
        'estimated_cost',
        'transport_cost',
    ];

    static protected array $rated_currency_fields = [
        'discount',
        'unit_price',
    ];

    static protected array $units_fields = [
        'qty_total',
        'qty_repair'
    ];

    static protected array $decision_fields = [
        'run_return',
        'confirm_return',
    ];

    static protected array $enums_fields = [
        'claim' => \Totem\SamComplaints\App\Enums\ClaimType::class,
        'status' => \Totem\SamComplaints\App\Enums\StatusType::class,
        'transport_method' => \Totem\SamComplaints\App\Enums\TransportType::class,
        'qc_approved' => \Totem\SamComplaints\App\Enums\AcceptanceType::class,
        'ceo_approved' => \Totem\SamComplaints\App\Enums\AcceptanceType::class,
        'sale_approved' => \Totem\SamComplaints\App\Enums\AcceptanceType::class,
    ];


    public function toArray($request): array
    {
        return [
            'created_at' => $this->resource->created_at->format('Y-m-d H:i:s'),
            'action' => $this->resource->action,
            'user' => $this->resource->user->fullname ?? '',
            'entries' => $this->resource->meta ? $this->setEntries($this->resource->meta): []
        ];
    }

    private function setEntries(array $entries): array
    {
        return array_merge(...array_filter(array_map([$this, 'prepareHistoryEntry'], $entries)));
    }

    private function prepareHistoryEntry(array $entry): ?array
    {
        if (in_array($entry['key'], self::$hide_fields, true)) {
            return null;
        }
        if (in_array($entry['key'], self::$currency_fields, true)) {
            return [$entry['key'] => $this->assignDefaultCurrency($entry['new'])];
        }
        if (in_array($entry['key'], self::$units_fields, true)) {
            return [$entry['key'] => $this->assignUnit($entry['new'])];
        }
        if (in_array($entry['key'], self::$decision_fields, true)) {
            return [$entry['key'] => $this->assignDecision($entry['new'])];
        }
        if (array_key_exists($entry['key'], self::$enums_fields)) {
            return [$entry['key'] => $this->assignEnum($entry['key'], $entry[$entry['key'] === 'status' ? 'old' : 'new'])];
        }
        return [$entry['key'] => $entry['new']];
    }

    private function assignDefaultCurrency(float $value): string
    {
        return $value .' zł';
    }

    private function assignUnit(int $value): string
    {
        return $value .' '.__('pcs');
    }

    private function assignDecision(int $value): string
    {
        return __($value === 0 ? 'No' : 'Yes');
    }

    private function assignEnum(string $key, ?int $value): string
    {
        /**@var \BenSampo\Enum\Enum $class */
        $class = self::$enums_fields[$key];

        return $class::getDescription($value);
    }

}
