<?php

namespace Totem\SamComplaints\App\Resources;

use Totem\SamCore\App\Resources\ApiCollection;

class ComplaintSimpleCollection extends ApiCollection
{

    public $collects = ComplaintSimpleResource::class;

}
