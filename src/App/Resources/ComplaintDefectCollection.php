<?php

namespace Totem\SamComplaints\App\Resources;

use Totem\SamCore\App\Resources\ApiCollection;

class ComplaintDefectCollection extends ApiCollection
{

    public $collects = ComplaintDefectResource::class;

}
