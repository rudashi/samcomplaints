<?php

namespace Totem\SamComplaints\App\Resources;

use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property-read \Totem\SamComplaints\App\Model\ComplaintDefect $resource
 */
class ComplaintExportResource extends ApiResource
{

    public function toArray($request = null): array
    {
        return [
            'reported_at'           => $this->resource->complaint->reported_at === null ? null : $this->resource->complaint->reported_at->format('Y-m-d'),
            'status'                => $this->resource->complaint->status->description,
            'complaint_number'      => $this->resource->complaint->complaint_number,
            'user'                  => $this->resource->complaint->user->fullname ?? null,
            'created'               => $this->resource->complaint->created_at->format('Y-m-d'),
            'order_number'          => $this->resource->complaint->order_number,
            'customer'              => $this->resource->complaint->customer,
            'order_name'            => $this->resource->complaint->order_name,
            'claim'                 => $this->resource->complaint->claim->description,
            'qty_total'             => $this->resource->complaint->qty_total,
            'description'           => $this->resource->complaint->description,
            'prime_income'          => $this->getNumberFormat($this->resource->complaint->prime_income),
            'total_discount'        => $this->getNumberFormat($this->resource->complaint->total_discount),
            'balance'               => $this->getNumberFormat($this->resource->complaint->balance),

            'defect'                => $this->resource->defect->name,
            'defect_place'          => $this->resource->defect->defect_place,
            'defect_machine'        => $this->resource->machine->name ?? null,
            'defect_user'           => $this->resource->user->fullname ?? null,
            'defect_description'    => $this->resource->description,
            'preventive_actions'    => $this->resource->preventive_actions,

        ];
    }

    private function getNumberFormat(?float $number): ?string
    {
        return $number !== null ? number_format($number, 2, '.', '') : null;
    }

}
