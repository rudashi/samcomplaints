<?php

namespace Totem\SamComplaints\App\Resources;

use JanRejnowski\SamDefects\App\Resources\DefectResource;
use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property-read \Totem\SamComplaints\App\Model\ComplaintDefect $resource
 */
class ComplaintDefectResource extends ApiResource
{

    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'defect' => DefectResource::make($this->whenLoaded('defect')),
            'machine' => $this->whenLoaded('machine', function () {
                return [
                    'id' => $this->resource->machine_id,
                    'name' => $this->resource->machine->name,
                ];
            }),
            'user' => $this->whenLoaded('user', function () {
                return [
                    'id' => $this->resource->user_id,
                    'fullname' => $this->resource->user->fullname,
                ];
            }),
            'description' => $this->resource->description,
            'note' => $this->resource->note,
            'preventive_actions' => $this->resource->preventive_actions,
            'status' => $this->resource->status,
            'opl' => $this->resource->opl,
            'summary_note' => $this->resource->summary_note,
            'summary_discount_type' => $this->resource->summary_discount_type,
            'summary_discount' => $this->resource->summary_discount,
            'protocol_at' => $this->resource->protocol_at === null ? null : $this->resource->protocol_at->format('Y-m-d'),
            'month_billing' => $this->resource->month_billing === null ? null : $this->resource->month_billing->format('Y-m'),
            'complaint' => ComplaintResource::make($this->whenLoaded('complaint')),
            'history' => HistoryResource::collection($this->whenLoaded('history')),
        ];
    }

}
