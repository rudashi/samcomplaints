<?php

namespace Totem\SamComplaints\App\Resources;

use Totem\SamCore\App\Resources\ApiCollection;

class ComplaintCollection extends ApiCollection
{

    public $collects = ComplaintResource::class;

}
