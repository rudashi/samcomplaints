<?php

namespace Totem\SamComplaints\App\Resources;

use JanRejnowski\SamDefects\App\Enums\DefectPlaceEnum;
use Totem\SamCore\App\Resources\ApiResource;

/**
 * @property-read \Totem\SamComplaints\App\Model\ComplaintDefect $resource
 */
class ComplaintReportResource extends ApiResource
{

    public function toArray($request): array
    {
        return [
            'complaint_id'  => $this->resource->complaint_id,
            'status'        => $this->resource->complaint->status->description,
            'qty_total'     => $this->resource->complaint->qty_total,
            'total_cost'    => $this->getNumberFormat($this->resource->complaint->total_cost),
            'claim'         => $this->resource->complaint->claim->description,
            'defect'        => $this->resource->defect->name,
            'defect_place'  => DefectPlaceEnum::getDescription($this->resource->defect->defect_place),
            'machine'       => $this->resource->machine->name ?? null,
            'user'          => $this->resource->user->fullname ?? null,
        ];
    }

}
