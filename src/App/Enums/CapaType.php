<?php

namespace Totem\SamComplaints\App\Enums;

use BenSampo\Enum\Enum;

class CapaType extends Enum
{

    public const Corrective = 'Corrective';
    public const Preventive = 'Preventive';
    public const Additional = 'Additional';

}
