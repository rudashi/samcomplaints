<?php

namespace Totem\SamComplaints\App\Enums;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Contracts\LocalizedEnum;

class TransportType extends Enum implements LocalizedEnum
{
    public const Courier    = 1;
    public const SelfPickup = 2;
    public const Client     = 3;

    public static function getLocalizationKey(): string
    {
        return 'sam-complaints::enums.' . static::class;
    }

}
