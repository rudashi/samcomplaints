<?php

namespace Totem\SamComplaints\App\Enums;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Contracts\LocalizedEnum;

class AcceptanceType extends Enum implements LocalizedEnum
{

    public const Decline    = 0;
    public const Accept     = 1;
    public const ToFix      = 2;
    public const Fixed      = 3;

    public static function getLocalizationKey(): string
    {
        return 'sam-complaints::enums.' . static::class;
    }

}