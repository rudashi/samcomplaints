<?php

namespace Totem\SamComplaints\App\Enums;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Contracts\LocalizedEnum;
use Totem\SamCore\App\Traits\CollectableEnum;

class StatusCapa extends Enum implements LocalizedEnum
{
    use CollectableEnum;

    public const Pending            = 40;
    public const During             = 42;
    public const Suspended          = 43;
    public const Done               = 44;
    public const Refused            = 48;

    public static function getLocalizationKey(): string
    {
        return 'sam-complaints::enums.' . static::class;
    }

    public function __toString(): string
    {
        return $this->description;
    }

}
