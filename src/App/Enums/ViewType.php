<?php

namespace Totem\SamComplaints\App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

class ViewType extends Enum implements LocalizedEnum
{

    public const COMPLAINT_DEFECT   = 'defect';
    public const DEFECT_TRANSPORT   = 'transport';

    public static function getLocalizationKey(): string
    {
        return 'sam-complaints::enums.' . static::class;
    }

}
