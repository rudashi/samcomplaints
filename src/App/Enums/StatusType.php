<?php

namespace Totem\SamComplaints\App\Enums;

use BenSampo\Enum\Enum;
use BenSampo\Enum\Contracts\LocalizedEnum;

class StatusType extends Enum implements LocalizedEnum
{
    public const Verification       = 25;
    public const QualityControl     = 10;
    public const Transport          = 50;
    public const SaleDirector       = 30;
    public const CEO                = 40;
    public const Shipping           = 51;
    public const Finance            = 60;
    public const Canceled           = 72;
    public const Rejected           = 71;
    public const Completed          = 88;
    public const ToSaleFix          = 1;
    public const ToFinished         = 5;
    public const ToQCFix            = 15;
    public const Subcontractor      = 17;

    public static function getLocalizationKey(): string
    {
        return 'sam-complaints::enums.' . static::class;
    }

    public function __toString(): string
    {
        return $this->description;
    }

}
