<?php

namespace Totem\SamComplaints\App\Traits;

use Totem\SamComplaints\App\Model\ComplaintDelivery;

trait ComplaintHasDeliveries
{

    public function deliveries(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(ComplaintDelivery::class, 'complaint');
    }

    public function attachDeliveries(array $records): \Illuminate\Database\Eloquent\Collection
    {
        return $this->deliveries()->createMany($records);
    }

    public function detachDeliveries(): void
    {
        $this->deliveries()->delete();
    }

    public function syncDeliveries(array $records): \Illuminate\Database\Eloquent\Collection
    {
        $key = $this->deliveries()->getRelated()->getKeyName();
        $ids = $this->newCollection($records)->pluck($key)->all();
        $current = $this->deliveries()->pluck($key)->all();
        $detach = array_diff($current, $ids);

        if (count($detach)) {
            $this->deliveries()->whereIn($key, $detach)->delete();
        }

        $instances = $this->newCollection();

        foreach ($records as $record) {
            $instances->push($this->deliveries()->updateOrCreate(
                [
                    $key => array_key_exists($key, $record) ? $record[$key] : -1,
                    $this->deliveries()->getForeignKeyName() => $this->deliveries()->getParentKey(),
                    $this->deliveries()->getMorphType() => $this->deliveries()->getMorphClass(),
                ],
                $record
            ));
        }

        return $instances;
    }

    public function duplicateDelivery(int $id, array $attributes = [])
    {
        return $this->deliveries()->save(
            $this->deliveries()
                ->firstWhere($this->deliveries()->getRelated()->getKeyName(), $id)
                ->replicate(['transport_cost', 'transport_method', 'delivery_date'])
                ->fill($attributes)
        );
    }

}
