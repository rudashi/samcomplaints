<?php

namespace Totem\SamComplaints\App\Traits;

use Totem\SamComplaints\App\Model\ComplaintDefect;

trait ComplaintHasDefects
{

    public function defects(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(ComplaintDefect::class, 'complaint');
    }

    public function attachDefects(array $records): \Illuminate\Database\Eloquent\Collection
    {
        return $this->defects()->createMany($records);
    }

    public function syncDefects(array $records): \Illuminate\Database\Eloquent\Collection
    {
        $this->defects()->delete();
        return $this->attachDefects($records);
    }

}
