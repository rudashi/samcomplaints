<?php

namespace Totem\SamComplaints\App\Traits;

use Totem\SamMessenger\App\Model\Thread;

trait ComplaintRelationships
{
    use ComplaintHasDefects,
        ComplaintHasDeliveries;

    public function user() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(app('config')->get('auth.providers.users.model'))->withTrashed();
    }

    public function thread(): \Illuminate\Database\Eloquent\Relations\MorphOne
    {
        return $this->morphOne(Thread::class, 'model');
    }

}
