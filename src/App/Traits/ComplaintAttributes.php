<?php

namespace Totem\SamComplaints\App\Traits;

use Illuminate\Support\Str;
use Illuminate\Support\Carbon;

trait ComplaintAttributes
{

    public function getDiscountCurrencyAttribute(): float
    {
        return round((float) $this->attributes['discount'] * (float) $this->attributes['currency_rate'], 2);
    }

    public function getUnitPriceCurrencyAttribute(): float
    {
        return round((float) $this->attributes['unit_price'] * (float) $this->attributes['currency_rate'], 2);
    }

    public function getCanModifyAttribute(): bool
    {
        return auth()->user()->can($this->attributes['modifiable']);
    }

    public function getDateLifeAttribute(): string
    {
        return (new Carbon())->diffInDays($this->{$this->getCreatedAtColumn()});
    }

    public function getQualityControlApprovedAttribute(): bool
    {
        return $this->attributes['qc_approved'] ?? false;
    }

    public function getSlugAttribute(): string
    {
        return Str::slug($this->attributes['complaint_number']);
    }

}