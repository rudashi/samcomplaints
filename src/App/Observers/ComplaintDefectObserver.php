<?php

namespace Totem\SamComplaints\App\Observers;

use Illuminate\Support\Facades\Notification;
use Totem\SamComplaints\App\Enums\AcceptanceType;
use Totem\SamComplaints\App\Model\ComplaintDefect;
use Totem\SamComplaints\App\Notifications\CapaOplNotification;

class ComplaintDefectObserver
{

    public function updating(ComplaintDefect $capa): void
    {
        if ($this->isOplNeed($capa)) {
            $capa->load('defect', 'complaint');

            Notification::route('mail', $this->getEmails($capa->defect->defect_place))->notify(new CapaOplNotification($capa));
        }
    }

    private function isOplNeed(ComplaintDefect $defect): bool
    {
        return $defect->getOriginal('opl') === AcceptanceType::Decline && $defect->getAttribute('opl') === AcceptanceType::Accept;
    }

    private function getEmails(int $defect_id): array
    {
        $emails = config('sam-defects.emails.'.$defect_id);

        if ($emails === null) {
            return [];
        }
        if (is_array($emails)) {
            return $emails;
        }
        return [$emails];
    }

}
