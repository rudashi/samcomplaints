<?php

namespace Totem\SamComplaints\App\Observers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Totem\SamComplaints\App\Enums\StatusType;
use Totem\SamComplaints\App\Exceptions\StatusValidationException;
use Totem\SamComplaints\App\Model\Complaint;
use Totem\SamComplaints\App\Services\ComplaintSummary;
use Totem\SamComplaints\App\Services\NotificationService;
use Totem\SamComplaints\App\Services\StatusRulesService;
use Totem\SamComplaints\App\Services\StatusService;

class ComplaintObserver
{

    private Request $request;
    private StatusService $status;
    private NotificationService $notification;
    private ComplaintSummary $summary;
    private Collection $rules;

    public function __construct(Request $request, StatusService $status, NotificationService $notification, ComplaintSummary $summary, StatusRulesService $rules)
    {
        $this->request      = $request;
        $this->status       = $status;
        $this->notification = $notification;
        $this->summary      = $summary;
        $this->rules        = $rules->validated($this->request->input('status'));
    }

    public function saving(Complaint $complaint): bool
    {
        if ($complaint->exists && $complaint->isDirty() === false && $complaint->isEditionNeedReturn()) {
            $complaint->setUpdatedAt($complaint->freshTimestamp());
        }

        if ($this->request->has('run_return_request') && $this->request->boolean('run_return_request')) {
            $this->notification->notifyOwner('run_return_request', $complaint);
        }

        return true;
    }

    public function creating(Complaint $complaint): void
    {
        $complaint->user_id             = auth()->id();
        $complaint->status              = $this->status->determine($complaint);
        $complaint->number              = $this->summary->getNewNumber();
        $complaint->complaint_number    = $this->summary->setNumber($complaint->number);
        $complaint->prime_income        = $this->summary::setPrimeIncome($complaint->total_price, $complaint->total_cost);
        $complaint->total_discount      = $this->summary::setTotalDiscount($complaint->discount, $complaint->estimated_cost, $complaint->currency_rate);
        $complaint->balance             = $this->summary::setBalance($complaint->prime_income, $complaint->total_discount);

        if ($this->request->boolean('run_return')) {
            $complaint->attachDeliveries($this->request->input('deliveries'));
        }

    }

    public function created(Complaint $complaint): void
    {
        $complaint->modifiable = $this->summary->setModifiablePermissions($complaint);

        if ($this->request->filled('files')) {
            $complaint->attachFiles($this->request->input('files'));
        }

        $this->summary->createThread($complaint);
        $this->notification->created($complaint);
    }

    public function updating(Complaint $complaint): bool
    {
        try {
            if ($this->rules->has('run_return_rest') && $this->rules->has('run_return_rest_qty') && $this->request->boolean('run_return_rest')) {
                $complaint->duplicateDelivery(
                    $this->request->input('run_return_rest_id'),
                    ['quantity' => $this->request->input('run_return_rest_qty')]
                );
            }

            $complaint->status = $this->status->determine($complaint);

            if (in_array(StatusType::ToSaleFix, [$complaint->status->value, (int) $this->request->input('status')], true)) {
                $complaint->prime_income        = $this->summary::setPrimeIncome($complaint->total_price, $complaint->total_cost);
                $complaint->total_discount      = $this->summary::setTotalDiscount($complaint->discount, $complaint->estimated_cost, $complaint->currency_rate);
                $complaint->balance             = $this->summary::setBalance($complaint->prime_income, $complaint->total_discount);
            }

            if ($this->rules->has('defects') && $this->request->filled('defects')) {
                $complaint->syncDefects($this->request->input('defects'));
            }
            if ($this->rules->has('run_return')) {
                $this->request->boolean('run_return')
                    ? $complaint->syncDeliveries($this->request->input('deliveries'))
                    : $complaint->detachDeliveries();
            }
            return true;

        } catch (StatusValidationException $e) {
            return false;
        }
    }

    public function updated(Complaint $complaint): void
    {
        $this->notification->updated($complaint);

        if ($this->request->filled('defects')) {
            $this->notification->noticeByDefect($complaint);
        }

        if (in_array($complaint->getOriginal('status'), [StatusType::Verification, StatusType::Transport], true)) {
            $this->notification->notifyShipping($complaint);
        }

        if (in_array($complaint->status->value, [StatusType::Completed, StatusType::Canceled, StatusType::Rejected], true)) {
            $this->summary->archiveThread($complaint);
        }
    }

    public function deleting(Complaint $complaint): void
    {
        $complaint->status = StatusType::Canceled;
    }

    public function deleted(Complaint $complaint): void
    {
        $this->notification->deleted($complaint);
    }

    public function retrieved(Complaint $complaint): void
    {
        $complaint->modifiable = $this->summary->setModifiablePermissions($complaint);

        $complaint->syncOriginalAttributes('modifiable');
    }

}
