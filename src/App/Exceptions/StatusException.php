<?php

namespace Totem\SamComplaints\App\Exceptions;

use Throwable;

class StatusException extends \LogicException
{

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message !== '' ? $message : 'Unpredictable status logic', $code, $previous);
    }

}