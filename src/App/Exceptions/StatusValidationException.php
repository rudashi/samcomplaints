<?php

namespace Totem\SamComplaints\App\Exceptions;

use Throwable;

class StatusValidationException extends \LogicException
{

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message !== '' ? $message : 'Validation status exception', $code, $previous);
    }

}