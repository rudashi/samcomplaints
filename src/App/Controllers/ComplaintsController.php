<?php

namespace Totem\SamComplaints\App\Controllers;

use Rudashi\LaravelHistory\Models\History;
use Totem\SamComplaints\App\Requests\ComplaintRequest;
use Totem\SamComplaints\App\Resources\HistoryResource;
use Totem\SamComplaints\App\Resources\ComplaintResource;
use Totem\SamCore\App\Resources\ApiCollection;
use Totem\SamCore\App\Resources\FileCollection;
use Totem\SamUsers\App\Resources\UserSimpleCollection;
use Totem\SamMachines\App\Resources\MachineSimpleCollection;
use Totem\SamComplaints\App\Resources\ComplaintSimpleCollection;
use Totem\SamCore\App\Repositories\Contracts\FileInterfaceRepository;
use Totem\SamUsers\App\Repositories\Contracts\UserRepositoryInterface;
use Totem\SamMachines\App\Repositories\Contracts\MachineRepositoryInterface;
use Totem\SamComplaints\App\Repositories\Contracts\ComplaintRepositoryInterface;
use Totem\SamCore\App\Controllers\ApiController;

class ComplaintsController extends ApiController
{

    public function __construct(ComplaintRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index(): ComplaintSimpleCollection
    {
        return new ComplaintSimpleCollection(
            $this->getFromRequestQuery($this->repository->allWithRelations(['user']))
        );
    }

    public function create(ComplaintRequest $request): ComplaintResource
    {
         return new ComplaintResource($this->repository->storeNew($request));
    }

    public function show(string $uuid): ComplaintResource
    {
        return new ComplaintResource(
            $this->getFromRequestQuery(
                $this->repository->canModify()->findWithRelationsByUuid($uuid, ['defects.defect', 'defects.user', 'defects.machine', 'deliveries'])
            )
        );
    }

    public function update(ComplaintRequest $request, string $uuid): ComplaintResource
    {
        return new ComplaintResource($this->repository->updateByUUID($request, $uuid));
    }

    public function delete(string $uuid): ComplaintResource
    {
        return new ComplaintResource($this->repository->deleteUUID($uuid));
    }

    public function files(string $uuid, FileInterfaceRepository $fileRepository): FileCollection
    {
        return new FileCollection(
            $fileRepository->findByModelUuid($uuid, get_class($this->repository->getModel()))
        );
    }

    public function history(string $uuid, History $history): ApiCollection
    {
        return HistoryResource::collection(
            $history->ofModel(get_class($this->repository->getModel()), $uuid)->with(['user'])->latest()->get()
        );
    }

    public function getUsers(UserRepositoryInterface $repository): UserSimpleCollection
    {
        return new UserSimpleCollection(
            $repository->all(['id', 'firstname', 'lastname'], 'firstname')
        );
    }

    public function getMachines(MachineRepositoryInterface $repository): MachineSimpleCollection
    {
        return new MachineSimpleCollection(
            $repository->all(['id', 'name', 'type'])
        );
    }

}
