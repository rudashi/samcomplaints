<?php

namespace Totem\SamComplaints\App\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use JanRejnowski\SamDefects\App\Enums\DefectPlaceEnum;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Totem\SamComplaints\App\Enums\StatusCapa;
use Totem\SamComplaints\App\Enums\StatusType;
use Totem\SamComplaints\App\Enums\ViewType;
use Totem\SamComplaints\App\Repositories\Contracts\ComplaintDefectRepositoryInterface;
use Totem\SamComplaints\App\Repositories\Contracts\ComplaintRepositoryInterface;
use Totem\SamComplaints\App\Resources\ComplaintDefectCollection;
use Totem\SamComplaints\App\Services\ReportService;
use Totem\SamCore\App\Controllers\ApiController;
use Totem\SamCore\App\Resources\ApiResource;
use Totem\SamCore\App\Services\DataTableFilters;

class ReportsComplaintsController extends ApiController
{

    private ComplaintRepositoryInterface $complaint;
    private ComplaintDefectRepositoryInterface $defect;

    public function __construct(ComplaintDefectRepositoryInterface $defect, ComplaintRepositoryInterface $complaint)
    {
        $this->defect = $defect;
        $this->complaint = $complaint;
    }

    public function index(Request $request)
    {
        switch ($request->query('type')) {
            case ViewType::DEFECT_TRANSPORT:
                return $this->auditByTransport();
            case ViewType::COMPLAINT_DEFECT:
                return $this->auditByDefects();
            case 'dash':
            case 'reports':
                return $this->reports($request);
            default:
                throw new BadRequestHttpException('Incorrect `type` parameter.');
        }
    }

    public function fetchFilters(Request $request): ApiResource
    {
        $table = new DataTableFilters($request->query('view'));
        $table->setViews(ViewType::toSelectArray());
        $table->setStatus(StatusType::toSelectArray());
        $table->setProperty('capa' , StatusCapa::toSelectArray());
        $table->setProperty('defect_place', DefectPlaceEnum::toSelectArray());
        $table->addHeader(ViewType::COMPLAINT_DEFECT, [
            [
                'text' => 'Complaint',
                'value' => 'complaint.complaint_number',
            ],
            [
                'text' => 'Employee',
                'value' => 'user.fullname',
                'customFilter' => 'user',
            ],
            [
                'text' => 'Machine',
                'value' => 'machine.name',
                'filterable' => false,
            ],
            [
                'text' => 'Defect place',
                'value' => 'defect.defect_place_name',
                'customFilter' => 'defect_place',
            ],
            [
                'text' => 'Defect',
                'value' => 'defect.name',
                'filterable' => false,
            ],
            [
                'text' => 'QC',
                'value' => 'complaint.qc_approved',
                'description' => 'Acceptance of Quality Control',
                'translate' => false,
                'filterable' => false,
            ],
            [
                'text' => 'Preventive actions',
                'value' => 'preventive_actions',
                'filterable' => false,
            ],
            [
                'text' => 'Summary',
                'value' => 'summary_note',
                'filterable' => false,
            ],
            [
                'text' => 'OPL',
                'value' => 'opl',
                'description' => 'Quality Alert',
                'translate' => false,
                'filterable' => false,
            ],
            [
                'text' => 'Protocol',
                'description' => 'Protocol',
                'value' => 'protocol_at',
                'filterable' => false,
            ],
            [
                'text' => 'Billing month',
                'description' => 'Billing month',
                'value' => 'month_billing',
                'filterable' => false,
            ],
            [
                'text' => 'Order number',
                'value' => 'complaint.order.order_number',
            ],
            [
                'text' => 'Order name',
                'value' => 'complaint.order.order_name',
            ],
            [
                'text' => 'Status',
                'value' => 'status.description',
                'customFilter' => 'status',
            ],
            [
                'text' => 'Actions',
                'value' => 'action',
                'sortable' => false,
                'filterable' => false,
            ],
        ]);
        $table->addHeader(ViewType::DEFECT_TRANSPORT, [
            [
                'text' => 'Complaint',
                'value' => 'complaint.complaint_number',
            ],
            [
                'text' => 'Defect',
                'value' => 'defect.name',
                'filterable' => false,
            ],
            [
                'text' => 'Order number',
                'value' => 'complaint.order.order_number',
            ],
            [
                'text' => 'Solution',
                'value' => 'summary_discount_type',
                'hide' => 'smAndUp',
                'sortable' => false,
                'filterable' => false,
            ],
            [
                'text' => 'Discount value',
                'value' => 'summary_discount',
                'sortable' => false,
                'filterable' => false,
            ],
            [
                'text' => 'Status',
                'value' => 'status.description',
                'customFilter' => 'status',
            ],
            [
                'text' => 'Actions',
                'value' => 'action',
                'sortable' => false,
                'filterable' => false,
            ],
        ]);
        return new ApiResource($table);
    }

    public function auditByDefects(): ComplaintDefectCollection
    {
        return new ComplaintDefectCollection(
            $this->getFromRequestQuery(
                $this->defect->queryAllByDefects(['complaint', 'defect', 'machine', 'user'])
            )
        );
    }

    public function auditByTransport(): ComplaintDefectCollection
    {
        return new ComplaintDefectCollection(
            $this->getFromRequestQuery(
                $this->defect->queryAllByDefects(['complaint', 'defect'])
                    ->whereHas('defect', function(Builder $query) {
                        $query->whereIn('defect_place', [DefectPlaceEnum::Transport]);
                    })
            )
        );
    }

    public function reports(Request $request): ApiResource
    {
        if (!$request->has('created_at') || strpos($request->query('created_at'), ',') === false) {
            throw new UnprocessableEntityHttpException(__('Date argument missing.'));
        }

        return new ApiResource(new ReportService(
            $this->defect,
            explode(',', $request->query('created_at')),
            get_class($this->complaint->getModel())
        ));

    }

}
