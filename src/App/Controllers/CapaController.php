<?php

namespace Totem\SamComplaints\App\Controllers;

use Illuminate\Http\Request;
use Rudashi\LaravelHistory\Models\History;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Totem\SamComplaints\App\Enums\CapaType;
use Totem\SamComplaints\App\Enums\StatusCapa;
use Totem\SamComplaints\App\Repositories\Contracts\ComplaintDefectRepositoryInterface;
use Totem\SamComplaints\App\Requests\CapaRequest;
use Totem\SamComplaints\App\Resources\ComplaintDefectResource;
use Totem\SamComplaints\App\Resources\HistoryResource;
use Totem\SamCore\App\Resources\ApiCollection;
use Totem\SamCore\App\Controllers\ApiController;
use Totem\SamCore\App\Resources\ApiResource;

class CapaController extends ApiController
{

    public function __construct(ComplaintDefectRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request): ApiResource
    {
        if ($request->has('status')) {
            return new ApiResource(StatusCapa::toCollection());
        }
        if ($request->has('type')) {
            return new ApiResource(CapaType::toArray());
        }
        throw new UnprocessableEntityHttpException(__('Action not recognized.'));
    }

    public function update(string $uuid, int $id, CapaRequest $request): ComplaintDefectResource
    {
        return new ComplaintDefectResource(
            $this->repository->updateComplaintAndId($request->validated(), $uuid, $id)
        );
    }

    public function history(int $id, History $history): ApiCollection
    {
        return HistoryResource::collection(
            $history->ofModel(get_class($this->repository->getModel()), $id)->with(['user'])->latest()->get()
        );
    }

}
