<?php

namespace Totem\SamComplaints\App\Controllers;

use Illuminate\Http\Request;
use Totem\SamCore\App\Resources\ApiCollection;
use Totem\SamCore\App\Services\CSVService;
use Totem\SamCore\App\Controllers\ApiController;
use Totem\SamComplaints\App\Resources\ComplaintExportResource;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;
use Totem\SamComplaints\App\Repositories\Contracts\ComplaintDefectRepositoryInterface;

class FileDownloadController extends ApiController
{

    public function __construct(ComplaintDefectRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function download(Request $request, CSVService $CSVService): \Symfony\Component\HttpFoundation\StreamedResponse
    {
        $data = $this->prepareData();

        switch ($request->getContentType()) {
            case 'text/csv':
            case null:
                return $CSVService->streamDownload($data->collection, 'file');
            default:
                throw new UnsupportedMediaTypeHttpException('File type not supported.');
        }
    }

    private function prepareData(): ApiCollection
    {
        return ComplaintExportResource::collection($this->getFromRequestQuery(
            $this->repository->queryAllByDefects([
                'defect:id,name,defect_place',
                'user:id,firstname,lastname',
                'machine:id,name',
                'complaint:uuid,reported_at,status,complaint_number,user_id,created_at,order_number,customer,order_name,claim,qty_total,description,prime_income,total_discount,balance',
                'complaint.user:id,firstname,lastname'
            ], [
                'defect_id',
                'complaint_type',
                'complaint_id',
                'machine_id',
                'user_id',
                'description',
                'preventive_actions',
            ])
        ));
    }

}
