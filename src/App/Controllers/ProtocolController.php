<?php

namespace Totem\SamComplaints\App\Controllers;

use Totem\SamComplaints\App\Repositories\Contracts\ComplaintRepositoryInterface;
use Totem\SamComplaints\App\Services\ProtocolService;
use Totem\SamCore\App\Controllers\ApiController;

class ProtocolController extends ApiController
{
    private ProtocolService $service;

    public function __construct(ComplaintRepositoryInterface $repository, ProtocolService $service)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    public function streamPDF(string $uuid): \Illuminate\Http\Response
    {
        return $this->service->streamPDF(
            $this->repository->canModify()->findWithRelationsByUuid($uuid, ['files']),
            $this->service->getComplaintDefectsByUuid($uuid)
        );
    }

}
