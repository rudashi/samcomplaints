<?php

namespace Totem\SamComplaints\App\Controllers;

use Totem\SamCore\App\Resources\FileResource;
use Totem\SamCore\App\Controllers\ApiController;
use Totem\SamComplaints\App\Requests\FileUploadRequest;
use Totem\SamCore\App\Repositories\Contracts\FileInterfaceRepository;
use Totem\SamComplaints\App\Repositories\Contracts\ComplaintRepositoryInterface;

class FileUploadController extends ApiController
{
    private FileInterfaceRepository $file;

    public function __construct(ComplaintRepositoryInterface $repository, FileInterfaceRepository $file)
    {
        $this->repository = $repository;
        $this->file = $file;
    }

    public function upload(string $uuid, FileUploadRequest $request)
    {
        try {
            return new FileResource(
                $this->file->saveFile($request, $this->repository->canModify()->findWithRelationsByUuid($uuid))
            );

        } catch (\Exception $exception) {
            return $this->response($this->error(422, $exception->getMessage()));
        }
    }

    public function remove(string $uuid, int $file_id) : FileResource
    {
        return new FileResource(
            $this->file->fileRemove($file_id, $this->repository->canModify()->findWithRelationsByUuid($uuid))
        );
    }

}
