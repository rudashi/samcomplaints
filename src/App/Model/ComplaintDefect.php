<?php

namespace Totem\SamComplaints\App\Model;

use Rudashi\LaravelHistory\Contracts\HasHistoryInterface;
use Rudashi\LaravelHistory\Traits\HasHistory;
use Totem\SamComplaints\App\Enums\StatusCapa;
use Totem\SamMachines\App\Model\Machine;
use JanRejnowski\SamDefects\App\Model\Defect;
use Totem\SamCore\App\Traits\CastsEnums;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property int complaint_id
 * @property int defect_id
 * @property int|null machine_id
 * @property int|null user_id
 * @property string description
 * @property string|null note
 * @property string|null preventive_actions
 * @property StatusCapa|null status
 * @property int opl
 * @property string|null summary_note
 * @property string|null summary_discount_type
 * @property string|null summary_discount
 * @property \DateTime|null protocol_at
 * @property \DateTime|null month_billing
 *
 * @property Defect defect
 * @property Complaint complaint
 * @property \App\User user
 * @property Machine machine
 */
class ComplaintDefect extends Model implements HasHistoryInterface
{
    use CastsEnums,
        HasHistory,
        SoftDeletes;

    protected $casts = [
        'protocol_at' => 'datetime',
        'month_billing' => 'date',
    ];

    protected array $enums = [
        'status' => StatusCapa::class,
    ];

    public function __construct(array $attributes = [])
    {
        $this->fillable([
            'defect_id',
            'complaint_type',
            'complaint_id',
            'machine_id',
            'user_id',
            'description',
            'note',
            'preventive_actions',
            'status',
            'opl',
            'summary_note',
            'summary_discount_type',
            'summary_discount',
            'protocol_at',
            'month_billing',
        ]);

        $this->setTable('complaints_defects');

        parent::__construct($attributes);
    }

    public function complaint(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo('complaint');
    }

    public function defect(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Defect::class)->withTrashed();
    }

    public function user() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(app('config')->get('auth.providers.users.model'))->withTrashed();
    }

    public function machine(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Machine::class)->withTrashed();
    }

    public function excludedHistoryAttributes(): array
    {
        return [
            'complaint_id',
            'complaint_type',
        ];
    }

    public function setMonthBillingAttribute(?string $value): void
    {
        $this->attributes['month_billing'] = $value ? $value.'-01' : null;
    }

}
