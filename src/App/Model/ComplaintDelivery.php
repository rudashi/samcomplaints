<?php

namespace Totem\SamComplaints\App\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Totem\SamComplaints\App\Enums\TransportType;
use Totem\SamCore\App\Traits\CastsEnums;

/**
 * @property int id
 * @property int complaint_id
 * @property string complaint_type
 * @property int|null shipping_id
 * @property string|null supplier
 * @property string quantity
 * @property string|null tracking_number
 * @property \Carbon\Carbon shipping_date
 * @property string|null name
 * @property string|null city
 * @property string|null postal_code
 * @property string|null street
 * @property string|null building_number
 * @property string|null suite_number
 * @property string|null country
 * @property string|null phone_number
 * @property string|null email
 * @property string|null description
 *
 * @property float|null transport_cost
 * @property string|null transport_method
 * @property \Carbon\Carbon|null delivery_date
 *
 * @property \Carbon\Carbon|null deleted_at
 *
 * @property string address
 *
 * @property Complaint complaint
 */
class ComplaintDelivery extends Model
{
    use CastsEnums,
        SoftDeletes;

    protected $casts = [
        'transport_cost'    => 'float',
        'quantity'          => 'int',
        'delivery_date'     => 'datetime',
        'shipping_date'     => 'datetime',
    ];

    protected array $enums = [
        'transport_method'  => TransportType::class,
    ];

    public function __construct(array $attributes = [])
    {
        $this->fillable([
            'complaint_type',
            'complaint_id',
            'shipping_id',
            'supplier',
            'quantity',
            'tracking_number',
            'shipping_date',
            'name',
            'city',
            'postal_code',
            'street',
            'building_number',
            'suite_number',
            'country',
            'phone_number',
            'email',
            'description',
            'transport_cost',
            'transport_method',
            'delivery_date',
        ]);

        $this->setTable('complaints_delivery');

        parent::__construct($attributes);
    }

    public function complaint(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo(Complaint::class);
    }

    public function getAddressAttribute(): string
    {
        return implode(' ', [
            $this->getAttributeFromArray('city'),
            $this->getAttributeFromArray('postal_code'),
            $this->getAttributeFromArray('street'),
            $this->getAttributeFromArray('building_number'),
        ]) . ($this->getAttributeFromArray('suite_number') ? '/'.$this->getAttributeFromArray('suite_number') : '');
    }

}
