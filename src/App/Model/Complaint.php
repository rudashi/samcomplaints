<?php

namespace Totem\SamComplaints\App\Model;

use Rudashi\Countries\Enums\CurrencyType;
use Totem\SamComplaints\App\Enums\ClaimType;
use Totem\SamComplaints\App\Enums\StatusType;
use Totem\SamComplaints\App\Enums\TransportType;
use Totem\SamComplaints\App\Enums\AcceptanceType;
use Totem\SamCore\App\Traits\HasUuid;
use Totem\SamCore\App\Traits\HasFiles;
use Totem\SamCore\App\Traits\CastsEnums;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rudashi\LaravelHistory\Traits\HasHistory;
use Totem\SamComplaints\App\Traits\ComplaintAttributes;
use Totem\SamComplaints\App\Traits\ComplaintRelationships;
use Rudashi\LaravelHistory\Contracts\HasHistoryInterface;
use Totem\SamCore\App\Repositories\Contracts\HasFilesInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property string uuid
 * @property \DateTime created_at
 * @property \DateTime updated_at
 * @property \DateTime|null reported_at
 * @property int user_id
 * @property StatusType status
 *
 * @property int number
 * @property string complaint_number
 * @property float prime_income
 * @property float total_discount
 * @property float balance
 * @property int|null order_id
 * @property string order_number
 * @property string order_name
 * @property string customer
 * @property int qty_total
 * @property float total_price
 * @property float total_cost
 * @property CurrencyType currency
 * @property float currency_rate
 * @property string description
 * @property ClaimType claim
 * @property bool run_return
 * @property float discount
 * @property float percentage
 * @property int qty_repair
 * @property float unit_price
 * @property float estimated_cost
 * @property string|null description_cost
 *
 * @property AcceptanceType|null qc_approved
 *
 * @property float|null transport_cost
 * @property string|null transport_method

 * @property AcceptanceType|null sale_approved
 * @property string|null sale_description
 *
 * @property AcceptanceType|null ceo_approved
 * @property string|null ceo_description
 *
 * @property bool|null confirm_return
 *
 * @property string|null invoice_number
 * @property string|null correction_invoice_number
 * @property string|null correction_invoice_value
 * @property string|null correction_order_number
 *
 * @property \DateTime|null deleted_at
 *
 * @property \App\User user
 * @property \Totem\SamMessenger\App\Model\Thread thread
 * @property \Illuminate\Database\Eloquent\Collection|ComplaintDefect[] defects
 * @property \Illuminate\Database\Eloquent\Collection|ComplaintDelivery[] deliveries
 * @property array modifiable
 * @property float discount_currency
 * @property float unit_price_currency
 * @property bool can_modify
 * @property string date_life
 * @property bool quality_control_approved
 * @property string slug
 */
class Complaint extends Model implements HasFilesInterface, HasHistoryInterface
{
    use SoftDeletes,
        HasUuid,
        CastsEnums,
        HasFiles,
        HasHistory,
        ComplaintAttributes,
        ComplaintRelationships;

    protected $casts = [
        'reported_at'               => 'datetime',
        'prime_income'              => 'float',
        'total_discount'            => 'float',
        'balance'                   => 'float',
        'qty_total'                 => 'int',
        'total_price'               => 'float',
        'total_cost'                => 'float',
        'currency_rate'             => 'float',
        'run_return'                => 'bool',
        'discount'                  => 'float',
        'percentage'                => 'float',
        'qty_repair'                => 'int',
        'unit_price'                => 'float',
        'estimated_cost'            => 'float',
        'transport_cost'            => 'float',
        'confirm_return'            => 'bool',
        'correction_invoice_value'  => 'float',
    ];

    protected array $enums = [
        'status'            => StatusType::class,
        'currency'          => CurrencyType::class,
        'claim'             => ClaimType::class,
        'transport_method'  => TransportType::class,
        'qc_approved'       => AcceptanceType::class,
        'sale_approved'     => AcceptanceType::class,
        'ceo_approved'      => AcceptanceType::class,
    ];

    public function __construct(array $attributes = [])
    {
        $this->addHidden([
            'number'
        ]);
        $this->fillable([
            'reported_at',
            'order_id',
            'order_number',
            'order_name',
            'customer',
            'qty_total',
            'total_price',
            'total_cost',
            'currency',
            'currency_rate',
            'description',
            'run_return',
            'claim',
            'discount',
            'percentage',
            'qty_repair',
            'unit_price',
            'estimated_cost',
            'description_cost',
            'invoice_number',

            'transport_cost',
            'transport_method',

            'qc_approved',

            'sale_approved',
            'sale_description',

            'ceo_approved',
            'ceo_description',

            'confirm_return',

            'correction_invoice_number',
            'correction_invoice_value',

            'correction_order_number',
        ]);

        $this->setKeyName('uuid');
        $this->setKeyType('string');

        parent::__construct($attributes);
    }

    public function isTransportFilled(): bool
    {
        if ($this->deliveries->count() === 0) {
            return $this->transport_method !== null && $this->transport_cost !== null;
        }
        return $this->deliveries->every(function (ComplaintDelivery $delivery) {
            return $delivery->transport_method !== null && $delivery->transport_cost !== null;
        });
    }

    public function isEditionReturnFilled(): bool
    {
        return $this->confirm_return !== null;
    }

    public function isQualityControlFilled(): bool
    {
        return $this->qc_approved !== null;
    }

    public function isEditionNeedReturn(): bool
    {
        return $this->run_return;
    }

    protected function runSoftDelete(): void
    {
        $query = $this->setKeysForSaveQuery($this->newModelQuery());

        $columns = ['status' => $this->attributes['status']];

        $query->update($columns);

        $this->syncOriginalAttributes(array_keys($columns));
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function path(): string
    {
        return 'complaints';
    }

}
