<?php

namespace Totem\SamComplaints;

use Totem\SamComplaints\App\Model\Complaint;
use Totem\SamComplaints\App\Model\ComplaintDefect;
use Totem\SamComplaints\App\Observers\ComplaintDefectObserver;
use Totem\SamComplaints\App\Observers\ComplaintObserver;
use Totem\SamCore\App\Traits\TraitServiceProvider;
use Illuminate\Support\ServiceProvider;

class SamComplaintsServiceProvider extends ServiceProvider
{

    use TraitServiceProvider;

    public function getNamespace() : string
    {
        return 'sam-complaints';
    }

    public function boot() : void
    {
        $this->loadAndPublish(
            __DIR__ . '/resources/lang',
            __DIR__ . '/database/migrations',
            __DIR__ . '/resources/views'
        );
        $this->publishes([
            __DIR__ . '/config/config.php' => config_path($this->getNamespace().'.php'),
        ], $this->getNamespace().'-config');
        $this->loadRoutesFrom(__DIR__.'/routes/api.php');
        $this->loadTranslationsFrom(__DIR__ . '/resources/lang',  $this->getNamespace());

        Complaint::observe(ComplaintObserver::class);
        ComplaintDefect::observe(ComplaintDefectObserver::class);
    }

    public function register() : void
    {
        $this->registerEloquentFactoriesFrom(__DIR__. '/database/factories');
        $this->mergeConfigFrom(__DIR__ . '/config/config.php', $this->getNamespace());

        $this->configureBinding([
            \Totem\SamComplaints\App\Repositories\Contracts\ComplaintRepositoryInterface::class => \Totem\SamComplaints\App\Repositories\ComplaintRepository::class,
            \Totem\SamComplaints\App\Repositories\Contracts\ComplaintDefectRepositoryInterface::class => \Totem\SamComplaints\App\Repositories\ComplaintDefectRepository::class,
        ]);
    }

}
