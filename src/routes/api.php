<?php

use Illuminate\Support\Facades\Route;
use Totem\SamComplaints\App\Controllers\CapaController;
use Totem\SamComplaints\App\Controllers\ComplaintsController;
use Totem\SamComplaints\App\Controllers\FileDownloadController;
use Totem\SamComplaints\App\Controllers\FileUploadController;
use Totem\SamComplaints\App\Controllers\ProtocolController;
use Totem\SamComplaints\App\Controllers\ReportsComplaintsController;

Route::group(['prefix' => 'api' ], static function() {

    Route::middleware(config('sam-admin.guard-api'))->group(static function() {

        Route::group(['prefix' => 'complaints'], static function() {

            Route::get('/', [ComplaintsController::class, 'index'])->middleware('permission:complaints.view');
            Route::post('/', [ComplaintsController::class, 'create'])->middleware('permission:complaints.create');
            Route::get('{uuid}', [ComplaintsController::class, 'show'])->middleware('permission:complaints.show');
            Route::get('{uuid}/files', [ComplaintsController::class, 'files'])->middleware('permission:complaints.show');
            Route::get('{uuid}/history', [ComplaintsController::class, 'history'])->middleware('permission:complaints.show');
            Route::patch('{uuid}', [ComplaintsController::class, 'update'])->middleware('permission:complaints.edit');
            Route::delete('{uuid}', [ComplaintsController::class, 'delete'])->middleware('permission:complaints.delete');

            Route::post('{uuid}/file', [FileUploadController::class, 'upload']);
            Route::delete('{uuid}/file/{fileId}', [FileUploadController::class, 'remove'])->where(['fileId' => '[0-9]+']);

            Route::group(['prefix' => 'capa'], static function() {
                Route::get('/', [CapaController::class, 'index'])->middleware('permission:complaints.capa');
            });
            Route::patch('{uuid}/defects/{id}', [CapaController::class, 'update'])->middleware('permission:complaints.capa');
            Route::get('{uuid}/defects/{id}/history', [CapaController::class, 'history'])->middleware('permission:complaints.show');

            Route::group(['prefix' => 'reports', 'middleware' => 'permission:complaints.reports'], static function() {
                Route::get('/', [ReportsComplaintsController::class, 'index']);
                Route::get('filters', [ReportsComplaintsController::class, 'fetchFilters']);
            });

            Route::get('users', [ComplaintsController::class, 'getUsers'])->middleware('permission:users.view');
            Route::get('machines', [ComplaintsController::class, 'getMachines'])->middleware('permission:complaints.status.10');

            Route::group(['prefix' => 'export'], static function() {
                Route::get('/', [FileDownloadController::class, 'download'])->middleware('permission:complaints.view');
            });

            Route::group(['prefix' => 'protocols'], static function() {
                Route::get('{uuid}/pdf', [ProtocolController::class, 'streamPDF']);
            });
        });

    });

});
