<!doctype html>
<html lang="pl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title></title>

    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow&subset=latin-ext" rel="stylesheet">
    <style >
        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            overflow: visible !important;
        }
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
            font-size: 1.2rem;
            font-family: 'PT Sans Narrow', sans-serif;
        }

        header {
            background: #ff0000;
            color: #FFFFFF;
            font-size: 1.5rem;
            text-align: center;
            height: 54mm;
        }
        header div:first-child {
            padding: 10px;
            border-bottom: 3px solid #FFFFFF;
        }
        header div:last-child {
            padding: 10px;
            border-top: 3px solid #FFFFFF;
        }

        section {
            width: 100%;
            height: 245mm;
        }
        section div {
            width: 60%;
            max-width: 60%;
            float: left;
            padding: .5rem 0;
        }
        section div:nth-child(odd) {
            width: 40%;
            max-width: 40%;
        }

        footer {
            display: flex;
            display: -webkit-box;
            justify-content: space-between;
            -webkit-box-pack: justify;
            margin-top: 15mm;
        }
        footer div {
            min-width: 25%;
            text-align: center;
        }
        footer div p {
            margin: 0;
        }
        footer div span {
            white-space: nowrap;
        }

        .page {
            page-break-after: always;
            display: -webkit-box;
            display: flex;
            -webkit-box-direction: normal;
            -webkit-box-orient: vertical;
            flex-direction: column;
            -webkit-box-pack: justify;
            justify-content: space-between;
        }
        .border {
            padding: 1rem;
            border: 3px solid red;
        }
        .uppercase {
            text-transform: uppercase;
        }
        .text--bold {
            font-weight: 700;
        }
        .text--red {
            color: red;
        }
        .text--display-1 {
            font-size: 2rem;
        }

    </style>
</head>
<body>
    <div class="page border">
        <header>
            <div></div>
            <h1 class="uppercase">{{ __('Quality Alert') }}</h1>
            <div></div>
        </header>
        <section>
                <div class="uppercase text--bold text--red text--display-1">{{ __('Department') }}</div>
                <div class="text--bold uppercase text--display-1">{{ $capa->defect->defect_place_name }}</div>

                <div class="uppercase text--bold text--red text--display-1">{{ __('Workplace') }}</div>
                <div class="text--display-1">{{ $capa->machine->name }}</div>

                <div class="uppercase text--bold text--display-1">{{ __('Order number') }}</div>
                <div class="text--bold text--display-1">{{ $complaint->order_number }}</div>

                @if ($capa->description)
                    <div class="uppercase text--bold text--display-1">{{ __('Description') }}</div>
                    <div>{!! $capa->description !!}</div>
                @endif

                @if ($capa->preventive_actions)
                    <div class="text--bold text--display-1">{{ __('Preventive actions') }}</div>
                    <div>{!! $capa->preventive_actions !!}</div>
                @endif
        </section>
        <footer>
            <div>
                <p>Data otwarcia</p>
                <span class="text--bold text--red">{{ $date->format('Y-m-d') }}</span>
            </div>
            <div>
                <p>Osoba odpowiedzialna</p>
                <span class="text--bold text--red">{{ $author }}</span>
            </div>
            <div>
                <p>Data zamknięcia</p>
                <span class="text--bold text--red">{{ $date->addDays(7)->format('Y-m-d') }}</span>
            </div>
        </footer>
    </div>
</body>
</html>
