<!doctype html>
<html lang="pl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title></title>

    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow&subset=latin-ext" rel="stylesheet">
    <style>
        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            overflow: visible !important;
        }
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
            font-size: 1rem;
            font-family: 'PT Sans Narrow', sans-serif;
        }
        header {
            font-size: 1.5rem;
            text-align: center;
        }
        header p {
            margin: 0;
            font-weight: normal;
        }
        header h1 {
            margin: 0;
        }
        dt {
            font-weight: 700;
        }
        dd {
            margin-bottom: .5rem;
            margin-left: 0;
        }
        section > p {
            text-align: right;
        }
        footer {
            display: flex;
            display: -webkit-box;
            justify-content: space-between;
            -webkit-box-pack: justify;
            margin-top: 15mm;
        }
        footer div {
            min-width: 25%;
            text-align: center;
        }
        footer div p {
            border-bottom: 1px dotted #000;
        }
        footer div span {
            white-space: nowrap;
        }
        .totem {
            height: 80px;
        }
        .totem.dark path {
            fill: #003b71 !important;
        }
        .page{
            page-break-after: always;
            display: -webkit-box;
            display: flex;
            -webkit-box-direction: normal;
            -webkit-box-orient: vertical;
            flex-direction: column;
            -webkit-box-pack: justify;
            justify-content: space-between;
        }
        .row {
            width: 100%;
            height: 95mm;
        }
        .col {
            float: left;
            padding: 10px;
            width: 30%;
            max-width: 30%;
        }
        img {
            max-height: 100%;
        }
    </style>
</head>
<body>
@foreach ($defects as $defect)
    <div class="page">
        <section>
            <p>{{ __('Document generated') }}: {{ $date }}</p>
            @svg('images/logo-totem.svg', 'totem dark')
            <hr/>
            <header>
                <p>{{ __('Complaint protocol') }}</p>
                <h1>{{ $complaint->complaint_number }}</h1>
            </header>
            <dl>
                <dt>{{ __('Order number') }}</dt>
                <dd>{{ $complaint->order_number }}</dd>

                <dt>{{ __('Order name') }}</dt>
                <dd>{{ $complaint->order_name }}</dd>

                <dt>{{ __('Defect') }}</dt>
                <dd>{{ $defect->defect->name }}</dd>

                @if ($defect->description)
                    <dt>{{ __('Description') }}</dt>
                    <dd>{!! $defect->description !!}</dd>
                @endif
            </dl>
            <div class="row">
                @foreach ($complaint->files as $file)
                    @if ($file->extension !== 'pdf')
                        <div class="col">
                            <img src="{{ asset('storage/'.$file->name) }}" alt=""/>
                        </div>
                    @endif
                @endforeach
            </div>
        </section>
        <footer>
            <div></div>
            <div>
                <p>&nbsp;</p>
                <span>Protokół sporządził {{ $author->fullname }}.</span>
            </div>
        </footer>
    </div>
@endforeach
</body>
</html>
