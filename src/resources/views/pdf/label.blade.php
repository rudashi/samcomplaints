<!doctype html>
<html lang="pl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Complaint Label - {{ $complaint->complaint_number }}</title>

    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow&subset=latin-ext" rel="stylesheet">
    <style>
        * {
            overflow: visible !important;
        }
        html, body {
            background-color: #ffffff;
            color: #000000;
            font-family:  'PT Sans Narrow', sans-serif;
            font-stretch: condensed;
            font-weight: 400;
            font-size: 2.5rem;
            margin: 0;
            padding: 3mm;
            height: 100%;
        }
        .table {
            width: 100%;
            display: flex;
            display: -webkit-box;
            flex-direction: column;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
        }
        .tr {
            width: 100%;
            display: -webkit-box;
            display: flex;
        }
        .tr .td:only-child {
            width: 100%;
        }
        .td {
            padding: 2mm 0 2mm 0;
            width: 50%;
        }
    </style>
</head>
<body>
    <div class="table">
        <div class="tr">
            <div class="td">
                <h1>{{ $complaint->complaint_number }}</h1>
            </div>
        </div>
        <div class="tr">
            <div class="td" style="text-align: left">
                <img src='data:image/png;base64,{{ (new \Milon\Barcode\DNS1D)->getBarcodePNG(\Illuminate\Support\Str::after($complaint->complaint_number, 'RMA/'), 'C39', 3, 150 ) }}' alt=""/>
            </div>
            <div class="td" style="text-align: right">
                <img src='data:image/png;base64,{{ (new \Milon\Barcode\DNS2D)->getBarcodePNG($url, 'QRCODE', 5, 5) }}' alt="" />
            </div>
        </div>
    </div>
</body>
</html>
