<?php

use Totem\SamComplaints\App\Enums\AcceptanceType;
use Totem\SamComplaints\App\Enums\ClaimType;
use Totem\SamComplaints\App\Enums\StatusCapa;
use Totem\SamComplaints\App\Enums\StatusType;
use Totem\SamComplaints\App\Enums\TransportType;
use Totem\SamComplaints\App\Enums\ViewType;

return [

    ClaimType::class => [
        ClaimType::QuotaDiscount        => __('Quota discount'),
        ClaimType::DiscountQty          => __('Discount qty'),
        ClaimType::Reprint              => __('Reprint'),
        ClaimType::RemoveDefects        => __('Remove defects'),
        ClaimType::CancelOrder          => __('Cancel order'),
        ClaimType::ReprintAndDiscount   => __('Reprint and discount'),
        ClaimType::NoteWithoutClaims    => __('Note without claims'),
        ClaimType::ToDetermine          => __('To determine'),
        ClaimType::PercentDiscount      => __('Percent discount'),
        ClaimType::DiscountNextOrder    => __('Discount for the next order'),
    ],

    StatusType::class => [
        StatusType::Verification        => __('Verification'),
        StatusType::QualityControl      => __('Quality Control'),
        StatusType::Transport           => __('Transport'),
        StatusType::SaleDirector        => __('Sale Director'),
        StatusType::Shipping            => __('Shipping'),
        StatusType::Finance             => __('Finance'),
        StatusType::ToSaleFix           => __('To CSO fix'),
        StatusType::ToQCFix             => __('To Quality Control fix'),
        StatusType::ToFinished          => __('To finished'),
        StatusType::Rejected            => __('Rejected'),
        StatusType::Canceled            => __('Canceled'),
        StatusType::Completed           => __('Completed'),
    ],

    TransportType::class => [
        TransportType::Courier          => __('Courier'),
        TransportType::SelfPickup       => __('Self pickup'),
        TransportType::Client           => __('Client'),
    ],

    AcceptanceType::class => [
        AcceptanceType::Decline         => __('Rejected'),
        AcceptanceType::Accept          => __('Accepted'),
        AcceptanceType::ToFix           => __('To be corrected'),
        AcceptanceType::Fixed           => __('Fixed'),
    ],

    StatusCapa::class => [
        StatusCapa::Pending             => __('Pending'),
        StatusCapa::During              => __('During'),
        StatusCapa::Suspended           => __('Suspended'),
        StatusCapa::Done                => __('Done'),
        StatusCapa::Refused             => __('Refused'),
    ],

    ViewType::class => [
        ViewType::COMPLAINT_DEFECT  => __('Defects'),
        ViewType::DEFECT_TRANSPORT  => __('Transport'),
    ]

];
