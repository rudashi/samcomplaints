<?php

namespace Totem\SamComplaints\Database\Seeds;

use Illuminate\Database\Seeder;
use Totem\SamMessenger\App\Model\Thread;

class ComplaintMessengerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() : void
    {
        if (config('app.env') !== 'production') {
            factory(Thread::class, 50)->state('complaint')->create();
        }
    }
}
