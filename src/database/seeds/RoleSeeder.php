<?php

namespace Totem\SamComplaints\Database\Seeds;

use Totem\SamAcl\App\Model\Role;
use Totem\SamAcl\App\Model\Permission;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{

    public function run() : void
    {
        Role::create([
            'slug' => 'complaints-salesman',
            'name' => 'Complaint as Salesman',
            'description' => 'Can edit complaint as Salesman',
        ])->attachPermissions(
            Permission::whereIn(
                'slug',
                ['complaints.status.1', 'complaints.status.5', 'complaints.view', 'complaints.edit', 'complaints.show', 'messenger.view']
            )->get()
        );

        Role::create([
            'slug' => 'complaints-qc',
            'name' => 'Complaint as Quality Control',
            'description' => 'Can edit complaint as Quality Control',
        ])->attachPermissions(
            Permission::whereIn(
                'slug',
                ['complaints.status.10', 'complaints.status.15', 'complaints.status.25', 'complaints.view', 'complaints.edit', 'complaints.show', 'defects.view', 'defects.show', 'defects.edit', 'defects.create', 'defects.delete', 'messenger.view']
            )->get()
        );

        Role::create([
            'slug' => 'complaints-transport',
            'name' => 'Complaint as Transport',
            'description' => 'Can edit complaint as Transport',
        ])->attachPermissions(
            Permission::whereIn(
                'slug',
                ['complaints.status.25', 'complaints.status.50', 'complaints.view', 'complaints.edit', 'complaints.show', 'messenger.view']
            )->get()
        );

        Role::create([
            'slug' => 'complaints-saledirector',
            'name' => 'Complaint as Sale Director',
            'description' => 'Can edit complaint as Sale Director',
        ])->attachPermissions(
            Permission::where(
                'slug',
                ['complaints.status.30', 'complaints.view', 'complaints.edit', 'complaints.show', 'messenger.view']
            )->get()
        );

        Role::create([
            'slug' => 'complaints-ceo',
            'name' => 'Complaint as CEO',
            'description' => 'Can edit complaint as CEO',
        ])->attachPermissions(
            Permission::where(
                'slug',
                ['complaints.status.40', 'complaints.view', 'complaints.edit', 'complaints.show', 'messenger.view']
            )->get()
        );

        Role::create([
            'slug' => 'complaints-shipping',
            'name' => 'Complaint as Shipping',
            'description' => 'Can edit complaint as Shipping',
        ])->attachPermissions(
            Permission::where(
                'slug',
                ['complaints.status.51', 'complaints.view', 'complaints.edit', 'complaints.show', 'messenger.view']
            )->get()
        );

        Role::create([
            'slug' => 'complaints-finance',
            'name' => 'Complaint as Finance',
            'description' => 'Can edit complaint as Finance',
        ])->attachPermissions(
            Permission::where(
                'slug',
                ['complaints.status.60', 'complaints.view', 'complaints.edit', 'complaints.show', 'messenger.view']
            )->get()
        );

    }

}
