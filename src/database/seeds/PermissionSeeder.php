<?php

namespace Totem\SamComplaints\Database\Seeds;

use Totem\SamComplaints\App\Enums\StatusType;
use Totem\SamAcl\Database\PermissionTraitSeeder;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{

    use PermissionTraitSeeder;

    /**
     * Array of permissions.
     * %action  : view|create|edit|show|delete|modify
     * %name    : translatable from JSON
     *
     * @return array
     *  [
     *      'slug' => 'roles.modify',
     *      'name' => 'Can Modify Roles',
     *      'description' => 'Can modify roles',
     *  ]
     */
    public function permissions(): array
    {
        $data =  [
            [
                'slug' => 'complaints.view',
                'name' => 'Can View Complaints',
                'description' => 'Can view complaints list',
            ],
            [
                'slug' => 'complaints.create',
                'name' => 'Can Create Complaint',
                'description' => 'Can create new complaint',
            ],
            [
                'slug' => 'complaints.edit',
                'name' => 'Can Edit Complaint',
                'description' => 'Can edit complaint',
            ],
            [
                'slug' => 'complaints.show',
                'name' => 'Can Show Complaint',
                'description' => 'Can show complaint',
            ],
            [
                'slug' => 'complaints.delete',
                'name' => 'Can Delete Complaint',
                'description' => 'Can delete complaint',
            ],
            [
                'slug' => 'complaints.reports',
                'name' => 'Can View Complaints reports',
                'description' => '',
            ],
            [
                'slug' => 'complaints.protocol',
                'name' => 'Pobieranie protokołów reklamacyjnych',
                'description' => '',
            ],
            [
                'slug' => 'complaints.capa',
                'name' => 'Zarządzanie CAPA reklamacjami',
                'description' => '',
            ],
        ];

        foreach (StatusType::toArray() as $key => $value) {
            $data[] = [
                'slug' => 'complaints.status.'.$value,
                'name' => 'Complaint Status '.$key,
                'description' => 'Can edit complaint with status '.$key,
            ];
        }

        return $data;

    }

}
