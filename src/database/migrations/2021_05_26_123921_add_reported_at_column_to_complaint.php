<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReportedAtColumnToComplaint extends Migration
{

    public function up(): void
    {
        try{
            if (!Schema::hasColumn('complaints', 'reported_at')) {
                Schema::table('complaints', function (Blueprint $table) {
                    $table->timestamp('reported_at')->nullable()->after('updated_at');
                });
            }
        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }

    }

    public function down(): void
    {
        if (Schema::hasColumn('complaints', 'reported_at')) {
            Schema::table('complaints', function (Blueprint $table) {
                $table->dropColumn('reported_at');
            });
        }
    }
}
