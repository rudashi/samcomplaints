<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAfterCapaToComplaintDefectTable extends Migration
{

    public function up(): void
    {
        try {
            Schema::table('complaints_defects', static function (Blueprint $table) {
                $table->longText('note')->nullable()->after('description');
                $table->integer('status')->default(\Totem\SamComplaints\App\Enums\StatusCapa::Pending)->after('preventive_actions');
                $table->tinyInteger('opl')->default(0)->after('status');        //One Point Lesson
                $table->longText('summary_note')->nullable()->after('opl');
                $table->longText('summary_discount_type')->nullable()->after('summary_note');
                $table->longText('summary_discount')->nullable()->after('summary_discount_type');
            });

        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }
    }

    public function down(): void
    {
        Schema::table('complaints_defects', static function (Blueprint $table) {
            $table->dropColumn([
                'note',
                'status',
                'opl',
                'summary_note',
                'summary_discount_type',
                'summary_discount',
            ]);
        });
    }

}
