<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCapaToComplaintDefectTable extends Migration
{

    public function up(): void
    {
        try{
            Schema::table('complaints_defects', function (Blueprint $table) {
                $table->timestamp('protocol_at')->nullable()->after('preventive_actions');
                $table->date('month_billing')->nullable()->after('protocol_at');
            });

        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }

    }

    public function down(): void
    {
        Schema::table('complaints_defects', function (Blueprint $table) {
            $table->dropColumn('protocol_at');
            $table->dropColumn('month_billing');
        });

    }
}
