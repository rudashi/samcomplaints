<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryAddressTable extends Migration
{

    public function up(): void
    {
        try {
            Schema::create('complaints_delivery', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();
                $table->uuidMorphs('complaint');

                $table->integer('shipping_id')->nullable();
                $table->string('supplier')->nullable();
                $table->string('quantity');
                $table->string('tracking_number')->nullable();
                $table->timestamp('shipping_date')->nullable();
                $table->string('name')->nullable();
                $table->string('city')->nullable();
                $table->string('postal_code')->nullable();
                $table->string('street')->nullable();
                $table->string('building_number')->nullable();
                $table->string('suite_number')->nullable();
                $table->string('country')->nullable();
                $table->string('phone_number')->nullable();
                $table->string('email')->nullable();
                $table->string('description')->nullable();

                $table->decimal('transport_cost', 10)->nullable();    // Transport
                $table->integer('transport_method')->nullable();            // Transport
                $table->timestamp('delivery_date')->nullable();             // Transport

                $table->softDeletes();
            });
        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }
    }

    public function down(): void
    {
        Schema::dropIfExists('complaints_delivery');
    }
}
