<?php

use Illuminate\Support\Facades\Artisan;
use Rudashi\Countries\Enums\CurrencyType;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComplaintsSetup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up() : void
    {
        try{
            Schema::create('complaints', static function (Blueprint $table) {
                $table->increments('id');                                               // Salesman
                $table->uuid('uuid')->unique();
                $table->timestamps();
                $table->integer('user_id')->unsigned();                                 // Salesman
                $table->integer('status');
                $table->integer('number')->unique();                                    // Salesman
                $table->string('complaint_number')->unique();                           // Salesman
                $table->decimal('prime_income', 10)->default('0.00');       // Salesman
                $table->decimal('total_discount', 10)->default('0.00');     // Salesman
                $table->decimal('balance', 10)->default('0.00');            // Salesman
                $table->integer('order_id')->nullable();                                // Salesman
                $table->string('order_number');                                         // Salesman
                $table->string('order_name');                                           // Salesman
                $table->string('customer');                                             // Salesman
                $table->integer('qty_total');                                           // Salesman
                $table->decimal('total_price', 10);                               // Salesman
                $table->decimal('total_cost', 10);                                // Salesman
                $table->string('currency')->default(CurrencyType::getKey(CurrencyType::PLN));                       // Salesman
                $table->decimal('currency_rate', 10, 4)->default('1.0000');      // Salesman
                $table->longText('description');                                        // Salesman
                $table->integer('claim');                                               // Salesman
                $table->tinyInteger('run_return')->default(0);                    // Salesman
                $table->decimal('discount', 10)->default('0.00');           // Salesman
                $table->decimal('percentage', 10)->default('0.00');         // Salesman
                $table->integer('qty_repair')->default('0');                      // Salesman
                $table->decimal('unit_price', 10)->default('0.00');         // Salesman
                $table->decimal('estimated_cost', 10)->default('0.00');     // Salesman
                $table->longText('description_cost')->nullable();                       // Salesman

                $table->tinyInteger('qc_approved')->nullable();                         // Quality Control

                $table->decimal('transport_cost', 10)->nullable();                // Transport
                $table->integer('transport_method')->nullable();                        // Transport

                $table->tinyInteger('sale_approved')->nullable();                       // Sale Director
                $table->longText('sale_description')->nullable();                       // Sale Director

                $table->tinyInteger('ceo_approved')->nullable();                        // CEO
                $table->longText('ceo_description')->nullable();                        // CEO

                $table->tinyInteger('confirm_return')->nullable();                      // Shipping

                $table->string('invoice_number')->nullable();                           // Salesman

                $table->string('correction_invoice_number')->nullable();                // Finance
                $table->decimal('correction_invoice_value', 10)->nullable();      // Finance

                $table->string('correction_order_number')->nullable();                  // Salesman

                $table->softDeletes();

                $table->foreign('user_id')->references('id')->on('users')
                    ->onUpdate('no action')->onDelete('no action');
            });

            Schema::create('complaints_defects', function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->timestamps();
                $table->integer('defect_id')->unsigned();
                $table->uuidMorphs('complaint');
                $table->integer('machine_id')->nullable()->unsigned();
                $table->integer('user_id')->nullable()->unsigned();
                $table->longText('description')->nullable();
                $table->longText('preventive_actions')->nullable();
                $table->softDeletes();

                $table->foreign('user_id')->references('id')->on('users')
                    ->onUpdate('no action')->onDelete('no action');

                $table->foreign('defect_id')->references('id')->on('defects')
                    ->onUpdate('no action')->onDelete('no action');

                $table->foreign('machine_id')->references('id')->on('machines')
                    ->onUpdate('no action')->onDelete('no action');
            });

        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }

        Artisan::call('db:seed', [
            '--class' => \Totem\SamComplaints\Database\Seeds\PermissionSeeder::class
        ]);

        Artisan::call('db:seed', [
            '--class' => \Totem\SamComplaints\Database\Seeds\RoleSeeder::class
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down() : void
    {
        (new \Totem\SamComplaints\Database\Seeds\PermissionSeeder)->down();

        Schema::dropIfExists('complaints_defects');
        Schema::dropIfExists('complaints');
    }
}
