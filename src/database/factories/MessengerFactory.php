<?php

use Faker\Generator as Faker;

/**
 * @var $factory \Illuminate\Database\Eloquent\Factory
 */

$factory->state(\Totem\SamMessenger\App\Model\Thread::class, 'complaint', function (Faker $faker) {

    return [
        'model_id' => $faker->numberBetween(1, 50),
        'model_type' => \Totem\SamComplaints\App\Model\Complaint::class,
    ];

});
