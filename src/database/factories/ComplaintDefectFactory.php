<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use JanRejnowski\SamDefects\App\Model\Defect;
use Totem\SamMachines\App\Model\Machine;

$factory->define(Totem\SamComplaints\App\Model\ComplaintDefect::class, function (Faker $faker) {
    return [
        'defect_id'             => Defect::query()->inRandomOrder()->first('id'),
        'machine_id'            => $faker->optional()->randomElement([Machine::query()->inRandomOrder()->first('id')]),
        'user_id'               => $faker->optional()->randomElement([User::query()->inRandomOrder()->first('id')]),
        'description'           => $faker->paragraph,
        'note'                  => $faker->optional()->paragraph,
        'preventive_actions'    => $faker->optional()->paragraph,
    ];
});
