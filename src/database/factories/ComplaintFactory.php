<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;

$factory->define(Totem\SamComplaints\App\Model\Complaint::class, function (Faker $faker) {
    $qty = $faker->numberBetween(1, 100000);
    return [
        'user_id'               => $faker->randomElement([User::query()->inRandomOrder()->first('id')]),
        'status'                => $faker->randomElement(\Totem\SamComplaints\App\Enums\StatusType::getValues()),
        'complaint_number'      => $faker->unique()->numerify('RMA/#####/2020'),
        'invoice_number'        => $faker->unique()->numerify('ISBN/###/##/2020'),
        'order_number'          => $faker->numerify('ZL/#####/2020'),
        'order_name'            => $faker->text(20),
        'customer'              => $faker->word,
        'qty_total'             => $qty,
        'total_price'           => $faker->randomFloat(2, 200, 500000),
        'total_cost'            => $faker->randomFloat(2, 200, 500000),
        'currency'              => $faker->randomElement(\Rudashi\Countries\Enums\CurrencyType::getKeys()),
        'currency_rate'         => $faker->randomFloat(2, 1, 6),
        'description'           => $faker->text(200),
        'claim'                 => $faker->randomElement(\Totem\SamComplaints\App\Enums\ClaimType::getValues()),
        'run_return'            => $faker->numberBetween(0, 1),
        'discount'              => $faker->randomFloat(2,0,50000),
        'percentage'            => $faker->randomFloat(2,0,10),
        'qty_repair'            => $faker->numberBetween(0, $qty),
        'unit_price'            => $faker->randomFloat(2,0, 40),
        'estimated_cost'        => $faker->randomFloat(2,0,100000),
        'description_cost'      => $faker->text(200),
    ];
});

$factory->state(Totem\SamComplaints\App\Model\Complaint::class, 'qc', function (Faker $faker) {
    return [
        'qc_approved' => $faker->numberBetween(0,1),
    ];
});
