<?php

use Illuminate\Foundation\Testing\WithFaker;
use Totem\SamComplaints\App\Enums\CapaType;
use Totem\SamAcl\Testing\AttachRoleToUserTrait;
use Totem\SamAdmin\Testing\ApiTest;
use Totem\SamComplaints\App\Enums\StatusCapa;
use Totem\SamComplaints\App\Model\ComplaintDefect;

class CapaTest extends ApiTest
{
    use AttachRoleToUserTrait;
    use WithFaker;

    protected string $endpoint = '/api/complaints';

    public function test_get_capa_without_query_should_return_422_response(): void
    {
        $this->get("$this->endpoint/capa")
            ->assertStatus(422)
            ->assertJsonFragment([
                'error' => [
                    'code' => 422,
                    'message' => __('Action not recognized.')
                ]
            ]);
    }

    public function test_get_capa_with_status_query_should_return_200_response(): void
    {
        $status = StatusCapa::fromValue(StatusCapa::Pending);

        $this->get("$this->endpoint/capa?status")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [],
                'apiVersion',
            ])
            ->assertJsonFragment([
                'value' => $status->value,
                'key' => $status->key,
                'description' => $status->description,
            ]);
    }

    public function test_get_capa_with_type_query_should_return_200_response(): void
    {
        $this->get("$this->endpoint/capa?type")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [],
                'apiVersion',
            ])
            ->assertJsonFragment([
                CapaType::getKey(CapaType::Corrective) => CapaType::Corrective,
                CapaType::getKey(CapaType::Preventive) => CapaType::Preventive,
                CapaType::getKey(CapaType::Additional) => CapaType::Additional,
            ]);
    }

    public function test_patch_capa_as_additional_should_return_200_response(): void
    {
        $defect = $this->getDataToCapa();

        $request = [
            'action' => CapaType::Additional,
            'protocol_at' => '2021-12-08',
            'month_billing' => '2021-12',
        ];
        $this->patch("$this->endpoint/$defect->complaint_id/defects/$defect->id", $request)
            ->assertOk()
            ->assertJsonStructure([
                'data' => [],
                'apiVersion',
            ])
            ->assertJsonFragment([
                'id' => $defect->id,
                'description' => $defect->description,
                'preventive_actions' => $defect->preventive_actions,
                'protocol_at' => $request['protocol_at'],
                'month_billing' => $request['month_billing'],
                'status' => null,
                'opl' => 0,
                'summary_note' => null,
                'summary_discount' => null,
            ]);
    }

    public function test_patch_capa_as_additional_with_invalid_data_should_not_save_and_return_200_response(): void
    {
        $defect = $this->getDataToCapa();

        $request = [
            'action' => CapaType::Additional,
            'opl' => 1
        ];
        $this->patch("$this->endpoint/$defect->complaint_id/defects/$defect->id", $request)
            ->assertOk()
            ->assertJsonStructure([
                'data' => [],
                'apiVersion',
            ])
            ->assertJsonFragment([
                'id' => $defect->id,
                'description' => $defect->description,
                'preventive_actions' => $defect->preventive_actions,
                'protocol_at' => null,
                'month_billing' => null,
                'status' => null,
                'opl' => 0,
                'summary_note' => null,
                'summary_discount_type' => null,
                'summary_discount' => null,
            ]);
    }

    public function test_patch_capa_as_corrective_should_return_200_response(): void
    {
        $defect = $this->getDataToCapa();

        $request = [
            'action' => CapaType::Corrective,
            'status' => StatusCapa::Done,
            'summary_note' => $this->faker->text,
            'summary_discount_type' => 'percentage',
            'summary_discount' => (string) $this->faker->randomDigit,
        ];
        $this->patch("$this->endpoint/$defect->complaint_id/defects/$defect->id", $request)
            ->assertOk()
            ->assertJsonStructure([
                'data' => [],
                'apiVersion',
            ])
            ->assertJsonFragment([
                'id' => $defect->id,
                'description' => $defect->description,
                'preventive_actions' => $defect->preventive_actions,
                'protocol_at' => null,
                'month_billing' => null,
                'status' => $request['status'],
                'opl' => 0,
                'summary_note' => $request['summary_note'],
                'summary_discount_type' => $request['summary_discount_type'],
                'summary_discount' => $request['summary_discount'],
            ]);
    }

    public function test_patch_capa_as_preventive_should_return_200_response(): void
    {
        $defect = $this->getDataToCapa();

        $request = [
            'action' => CapaType::Preventive,
            'status' => StatusCapa::Suspended,
            'opl' => 1,
            'summary_note' => $this->faker->text,
            'summary_discount_type' => 'percentage',
            'summary_discount' => (string) $this->faker->randomDigit,
        ];
        $this->patch("$this->endpoint/$defect->complaint_id/defects/$defect->id", $request)
            ->assertOk()
            ->assertJsonStructure([
                'data' => [],
                'apiVersion',
            ])
            ->assertJsonFragment([
                'id' => $defect->id,
                'description' => $defect->description,
                'preventive_actions' => $defect->preventive_actions,
                'protocol_at' => null,
                'month_billing' => null,
                'status' => $request['status'],
                'opl' => 1,
                'summary_note' => $request['summary_note'],
                'summary_discount_type' => null,
                'summary_discount' => $request['summary_discount'],
            ]);
    }

    public function test_patch_capa_without_action_should_failed_and_return_422_response(): void
    {
        $defect = $this->getDataToCapa();

        $this->patch("$this->endpoint/$defect->complaint_id/defects/$defect->id", [])
            ->assertStatus(422)
            ->assertJsonFragment([
                'error' => [
                    'code' => 422,
                    'message' => [
                        'action' => ['The action field is required.']
                    ]
                ],
            ]);
    }

    public function test_patch_capa_with_invalid_action_should_failed_and_return_422_response(): void
    {
        $defect = $this->getDataToCapa();

        $request = [
            'action' => 'abc',
        ];
        $this->patch("$this->endpoint/$defect->complaint_id/defects/$defect->id", $request)
            ->assertStatus(422)
            ->assertJsonFragment([
                'error' => [
                    'code' => 422,
                    'message' => [
                        'action' => [__('The :attribute field value you have entered is invalid.', ['attribute' => 'action'])]
                    ]
                ],
            ]);
    }

    /**  @return ComplaintDefect|\Illuminate\Database\Eloquent\Model */
    private function getDataToCapa(): ComplaintDefect
    {
        return ComplaintDefect::query()
            ->firstWhere('complaint_id', '28d08522-1f79-40c9-8ea7-902dc972e4e8');
    }

}
