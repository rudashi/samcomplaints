<?php

use Illuminate\Notifications\AnonymousNotifiable;
use Illuminate\Support\Facades\Notification;
use JanRejnowski\SamDefects\App\Enums\DefectPlaceEnum;
use Totem\SamComplaints\App\Enums\StatusType;
use Totem\SamComplaints\App\Model\Complaint;
use Totem\SamComplaints\App\Model\ComplaintDefect;
use Totem\SamComplaints\App\Notifications\ComplaintSubcontractor;
use Totem\SamComplaints\App\Notifications\DamagedDuringTransportNotify;
use Totem\SamComplaints\App\Notifications\DefectBySubcontractor;
use Totem\SamComplaints\App\Services\NotificationService;

class DefectNotificationTest extends \Tests\TestCase
{
    use \Illuminate\Foundation\Testing\DatabaseTransactions;

    private NotificationService $service;

    private int $transport = 64;
    private int $subcontractor = 69;

    protected function setUp(): void
    {
        parent::setUp();
        $this->actingAs(factory(\App\User::class)->create());
        $this->service = app(NotificationService::class);
        $this->permission = app(\Totem\SamAcl\App\Repositories\Contracts\PermissionRepositoryInterface::class);
    }

    public function test_send_notification_to_defect_place_owner(): void
    {
        Notification::fake();

        $complaint = $this->setComplaint();
        $complaint->attachDefects(factory(ComplaintDefect::class, 2)->make(['defect_id' => $this->transport])->toArray());
        $complaint->attachDefects(factory(ComplaintDefect::class, 2)->make(['defect_id' => $this->subcontractor])->toArray());

        $this->service->noticeByDefect($complaint);

        Notification::assertSentTo(
            new AnonymousNotifiable,
            \Totem\SamComplaints\App\Notifications\CapaActionNeededNotify::class,
        );

        Notification::assertSentTo(
            new AnonymousNotifiable,
            DefectBySubcontractor::class,
            static function (DefectBySubcontractor $notification, array $channels, AnonymousNotifiable $notifiable) {
                return $notifiable->routes['mail'] === config('sam-defects.emails.'.DefectPlaceEnum::Subcontractor);
            }
        );

        Notification::assertSentTo(
            $this->getUsers(StatusType::Finance),
            ComplaintSubcontractor::class,
            static function (ComplaintSubcontractor $notification, array $channels, $notifiable) {
                return in_array($notifiable->email, ['borys.zmuda@totem.com.pl', 'adres@totem.com.pl']);
            }
        );

        Notification::assertSentTo(
            $this->getUsers(StatusType::Transport),
            DamagedDuringTransportNotify::class,
            static function (DamagedDuringTransportNotify $notification, array $channels, $notifiable) {
                return $notifiable->email === 'borys.zmuda@totem.com.pl';
            }
        );

    }

    private function setComplaint(): Complaint
    {
        Complaint::getEventDispatcher()->forget('eloquent.saving: '.Complaint::class);
        Complaint::getEventDispatcher()->forget('eloquent.created: '.Complaint::class);
        Complaint::getEventDispatcher()->forget('eloquent.updating: '.Complaint::class);
        Complaint::getEventDispatcher()->forget('eloquent.updated: '.Complaint::class);
        Complaint::getEventDispatcher()->forget('eloquent.deleting: '.Complaint::class);
        Complaint::getEventDispatcher()->forget('eloquent.deleted: '.Complaint::class);
        Complaint::getEventDispatcher()->forget('eloquent.retrieved: '.Complaint::class);

        /** @var Complaint $model */
        $model = factory(Complaint::class)->create(['status' => null]);
        $model->attachDefects(factory(ComplaintDefect::class, 1)->make()->toArray());

        return $model;
    }

    private function getUsers(string $context): \Illuminate\Database\Eloquent\Collection
    {
        $query = $this->permission->findBySlug('complaints.status.'.$context, ['users', 'roles.users'], ['id']);
        return $query->users->merge($query->roles->pluck('users')->collapse());
    }
}
